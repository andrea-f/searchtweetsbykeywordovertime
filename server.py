#!/usr/bin/env python
from BaseHTTPServer import BaseHTTPRequestHandler
import urlparse
import SocketServer
from PolitifyEngine import PolitifyEngine
import os, sys
import json
import cgi

class GetHandler(BaseHTTPRequestHandler):
    
    def do_GET2(self):
        parsed_path = urlparse.urlparse(self.path)
        message_parts = [
                'CLIENT VALUES:',
                'client_address=%s (%s)' % (self.client_address,
                                            self.address_string()),
                'command=%s' % self.command,
                'path=%s' % self.path,
                'real path=%s' % parsed_path.path,
                'query=%s' % parsed_path.query,
                'request_version=%s' % self.request_version,
                '',
                'SERVER VALUES:',
                'server_version=%s' % self.server_version,
                'sys_version=%s' % self.sys_version,
                'protocol_version=%s' % self.protocol_version,
                '',
                'HEADERS RECEIVED:',
                ]
        for name, value in sorted(self.headers.items()):
            message_parts.append('%s=%s' % (name, value.rstrip()))
        message_parts.append('')
        message = '\r\n'.join(message_parts)
        self.send_response(200)
        self.end_headers()
        self.wfile.write(message)
        return

    def do_GET(self):
        self.parsed_path = urlparse.urlparse(self.path)
    	self.send_response(200)
    	print self.path
    	lib = os.path.abspath(os.path.join(os.path.dirname( __file__ )))

    	if "app" in self.path:
    		loc = "/webapp/index.html"
    		
    		f = open(lib+loc)
    		content = f.read()
    		f.close()
        if "addrecord" in self.path:
            print self.path, "inp"
            loc = "/webapp/input.html"
            f = open(lib+loc)
            content = f.read()
            f.close()
    	elif "entry" in self.path:
    		content = self._processRequestObject()
    		self.send_header('Content-Type', 'application/json')

        if self.path.endswith('.map') or self.path.endswith('.ico') or self.path.endswith('.js') or self.path.endswith('.css') or self.path.endswith('.png') or self.path.endswith('.jpg'):
        	try:
	        	f = open(lib+"/webapp"+self.path)
	        	content = f.read()
	        	f.close()
	        except Exception as e:
	        	print "[ERROR][do_GET] Fetching file: %s" % e
	        	content ="There was an error: %s" % e 

        #if not localhost:
        self.send_header('Access-Control-Allow-Origin', '*')
        self.end_headers()
        self.wfile.write(content)

	def do_POST(self):
		self.send_response(200)
		#print "DOING A POST"
		content = self._processRequestObject()
		self.send_header('Content-Type', 'application/json')
		self.end_headers()
		self.wfile.write(content)
        
    def _processRequestObject(self):
    	video = self._getAllEntries()
        try:
        	video_json = json.dumps(video, ensure_ascii=True, sort_keys=True, indent=4)
        except Exception as e:
            print "ERROR: %s" % e
            print type(video)
            video_json = video
        print len(video)
    	return video_json
        #callback = request.args.get('callback')
        #d = json.dumps(dict(a='foo'))
        #return 'some_func(' + d + ');'
        #return '{0}({1})'.format(callback, {'a':1, 'b':2})
        
    def _getEntries(self):
    	"""Return all entries from db"""
    	prog = self.mockInput()
    	pe = PolitifyEngine(record = prog)
    	video = pe.run(silent=True)
        #print video.img_var
    	video_pop = {}
        for key, val in video.__dict__.items():
            if not val is None:
                try:
                    if not len(val) == 0:
                        video_pop[key]=val
                except:
                    video_pop[key]= val
    	return [video_pop]

    def _getAllEntries(self):
        """Get all entries from mongoDB."""
        pe = PolitifyEngine()
        all_videos = pe.getAllEntries(coll = "videos")
        vids = []
	#video_pop = {}
        try:
            for vid in all_videos:
               	 video_pop = {}	
		 for key, val in vid.items():
                    if not val is None and not "_id" in key:
                        try:
                            if not len(val) == 0 and not "text" in key:
                                video_pop[key]=val
                        except:
                            video_pop[key]= val
		 vids.append(video_pop)
        except Exception as e:
            print "[_getAllEntries] ERROR: %s" % e
        
        #return [video_pop]
	return vids


        #print all_videos
        #return all_videos
        
    def mockInput(self):

		return [{
                "title":"Otto e Mezzo",
                "date": "17-06-2015",
                "url":"https://www.youtube.com/watch?v=vYECZ2PFY7A",
                "cp":"la7",
                "start_time":"20:30:00",
                "keywords":["De Gregorio"],
                "description":"Ospiti di Lilli Gruber Fabrizio Barca (Partito Democratico), Concita De Gregorio (La Repubblica) e Paolo Mieli (Presidente RCS Libri)."
        }]

    def do_POST(self):
        # Parse the form data posted
        form = cgi.FieldStorage(
            fp=self.rfile, 
            headers=self.headers,
            environ={'REQUEST_METHOD':'POST',
                     'CONTENT_TYPE':self.headers['Content-Type'],
                     })

        # Begin the response
        self.send_response(200)
        self.end_headers()
        self.wfile.write('Client: %s\n' % str(self.client_address))
        self.wfile.write('User-agent: %s\n' % str(self.headers['user-agent']))
        self.wfile.write('Path: %s\n' % self.path)
        self.wfile.write('Form data:\n')

        # Echo back information about what was posted in the form
        for field in form.keys():
            field_item = form[field]
            if field_item.filename:
                # The field contains an uploaded file
                file_data = field_item.file.read()
                file_len = len(file_data)
                del file_data
                self.wfile.write('\tUploaded %s as "%s" (%d bytes)\n' % \
                        (field, field_item.filename, file_len))
            else:
                # Regular form value
                self.wfile.write('\t%s=%s\n' % (field, form[field].value))
        return
    

if __name__ == '__main__':
    from BaseHTTPServer import HTTPServer
    try:
        ip = sys.argv[1]
    except:
        #ip = "80.241.222.199"
        ip = "localhost"

    try:
        port = sys.argv[2]
    except:
        port = 8989

    print ip, port    
    #print os.environ['HTTP_HOST'], os.environ['PATH_INFO']
    server = HTTPServer((str(ip), int(port)), GetHandler)
    print 'Starting server, use <Ctrl-C> to stop'
    print 'Serving on %s at port %s' % (ip, port)
    server.serve_forever()
