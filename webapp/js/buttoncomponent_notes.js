// Simple pure-React component so we don't have to remember
// Bootstrap's classes
var BootstrapButton = React.createClass({
  render: function() {
    // transferPropsTo() is smart enough to merge classes provided
    // to this component.
    return this.transferPropsTo(
      <a href="javascript:;" role="button" className="btn">
        {this.props.children}
      </a>
    );
  }
});

var BootstrapModal = React.createClass({
  // The following two methods are the only places we need to
  // integrate with Bootstrap or jQuery!
  componentDidMount: function() {
    // When the component is added, turn it into a modal
    $(this.getDOMNode())
      .modal({backdrop: 'static', keyboard: false, show: false})
  },
  componentWillUnmount: function() {
    $(this.getDOMNode()).off('hidden', this.handleHidden);
  },
  close: function() {
    $(this.getDOMNode()).modal('hide');
  },
  open: function() {
    $(this.getDOMNode()).modal('show');
  },
  render: function() {
    var confirmButton = null;
    var cancelButton = null;

    if (this.props.confirm) {
      confirmButton = (
        <BootstrapButton
          onClick={this.handleConfirm}
          className="btn-primary">
          {this.props.confirm}
        </BootstrapButton>
      );
    }
    if (this.props.cancel) {
      cancelButton = (
        <BootstrapButton onClick={this.handleCancel}>
          {this.props.cancel}
        </BootstrapButton>
      );
    }

    return (
      <div className="modal hide fade">
        <div className="modal-header">
          <button
            type="button"
            className="close"
            onClick={this.handleCancel}
            dangerouslySetInnerHTML={{__html: '&times'}}
          />
          <h3>{this.props.title}</h3>
        </div>
        <div className="modal-body">
          {this.props.children}
        </div>
        <div className="modal-footer">
          {cancelButton}
          {confirmButton}
        </div>
      </div>
    );
  },
  handleCancel: function() {
    if (this.props.onCancel) {
      this.props.onCancel();
    }
  },
  handleConfirm: function() {
    if (this.props.onConfirm) {
      this.props.onConfirm();
    }
  }
});

var Example = React.createClass({
  handleCancel: function() {
    if (confirm('Are you sure you want to cancel?')) {
      this.refs.modal.close();
    }
  },
  render: function() {
    var modal = null;
    modal = (
      <BootstrapModal
        ref="modal"
        confirm="OK"
        cancel="Cancel"
        onCancel={this.handleCancel}
        onConfirm={this.closeModal}
        title="Hello, Bootstrap!">
          This is a React component powered by jQuery and Bootstrap!
      </BootstrapModal>
    );
    return (
      <div className="example">
        {modal}
        <BootstrapButton onClick={this.openModal}>Open modal</BootstrapButton>
      </div>
    );
  },
  openModal: function() {
    this.refs.modal.open();
  },
  closeModal: function() {
    this.refs.modal.close();
  }
});

// In this example we also have two components - a picture and
// a picture list. The pictures are fetched from Instagram via AJAX.
<div className="container-fluid">
              <div className="panel panel-info">

                  <div className="panel-heading">
                    Politify analysed videos
                  </div>

                  <div className="panel-body">
                    
                      <div className="row">
                        <div className="col-md-4">                          
                          <div className="entries"> {entries} </div>                      
                        </div>
                        <div className="col-md-8">                          
                            <div className="favorites"> {favorites} </div>
                             <div className="video_player embed-responsive-item"></div>
                        </div>
                      </div>

                    
                  </div>


              </div>
              
              
          </div>
          .map(function(x,i){
                return [{x[i]}];
              })}



          <iframe src="http://www.rai.tv/dl/RaiTV/programmi/media/ContentItem-b048f381-a0da-40e2-bff4-e55b85d66a84.html?iframe" 
            style="border:0px; padding: 0px; margin:0px; width: 100%; height: 100%; min-width: 355px; min-height: 200px;" 
            allowfullscreen="true"
            webkitallowfullscreen="true"
            mozallowfullscreen="true"
            scrolling="no">
          </iframe>


          <embed id="me_flash_0"
           name="me_flash_0" play="true" loop="false" quality="high" bgcolor="#000000" 
           wmode="transparent" 
           allowscriptaccess="always" 
           allowfullscreen="true" 
           type="application/x-shockwave-flash"
           pluginspage="//www.macromedia.com/go/getflashplayer" 
           src="http://www.rai.tv/dl/player/Fallback/flashmediaelement.swf" 
           flashvars="id=me_flash_0&amp;isvideo=true&amp;autoplay=true&amp;preload=none&amp;width=968&amp;startvolume=0.8&amp;timerrate=250&amp;flashstreamer=&amp;height=580&amp;pseudostreamstart=start&amp;autohideinterval=5&amp;islive=false&amp;smoothing=true" 
           width="968" height="580" nanclass="mejs-shim"
          >