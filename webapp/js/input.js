/** @jsx React.DOM */

// Simple pure-React component so we don't have to remember
// Bootstrap's classes
var BootstrapButton = React.createClass({
  render: function() {
    // transferPropsTo() is smart enough to merge classes provided
    // to this component.
    return this.transferPropsTo(
      <a href="javascript:;" role="button" className="btn">
        {this.props.children}
      </a>
    );
  }
});
//<span class="label label-primary">Primary</span>
var BootstrapLabel = React.createClass({
  render: function() {
    // transferPropsTo() is smart enough to merge classes provided
    // to this component.
    return this.transferPropsTo(
      <span className="label label-primary">
        {this.props.children}
      </span>
    );
  }
});





var BootstrapModal = React.createClass({
  // The following two methods are the only places we need to
  // integrate with Bootstrap or jQuery!
  componentDidMount: function() {
    // When the component is added, turn it into a modal
    $(this.getDOMNode())
      .modal({backdrop: 'static', keyboard: false, show: false})
  },
  componentWillUnmount: function() {
    $(this.getDOMNode()).off('hidden', this.handleHidden);
  },
  close: function() {
    $(this.getDOMNode()).modal('hide');
  },
  open: function() {
    $(this.getDOMNode()).modal('show');
  },
  render: function() {
    var confirmButton = null;
    var cancelButton = null;

    if (this.props.confirm) {
      confirmButton = (
        <BootstrapButton
          onClick={this.handleConfirm}
          className="btn-primary">
          {this.props.confirm}
        </BootstrapButton>
      );
    }
    if (this.props.cancel) {
      cancelButton = (
        <BootstrapButton onClick={this.handleCancel}>
          {this.props.cancel}
        </BootstrapButton>
      );
    }

    return (
      <div className="modal hide fade">
        <div className="modal-header">
          <button
            type="button"
            className="close"
            onClick={this.handleCancel}
            dangerouslySetInnerHTML={{__html: '&times'}}
          />
          <h3>{this.props.title}</h3>
        </div>
        <div className="modal-body">
          {this.props.children}
        </div>
        <div className="modal-footer">
          {cancelButton}
          {confirmButton}
        </div>
      </div>
    );
  },
  handleCancel: function() {
    if (this.props.onCancel) {
      this.props.onCancel();
    }
  },
  handleConfirm: function() {
    if (this.props.onConfirm) {
      this.props.onConfirm();
    }
  }
});

var Example = React.createClass({
  handleCancel: function() {
    if (confirm('Are you sure you want to cancel?')) {
      this.refs.modal.close();
    }
  },
  render: function() {
    var modal = null;
    modal = (
      <BootstrapModal
        ref="modal"
        confirm="OK"
        cancel="Cancel"
        onCancel={this.handleCancel}
        onConfirm={this.closeModal}
        title="Hello, Bootstrap!">
          This is a React component powered by jQuery and Bootstrap!
      </BootstrapModal>
    );
    return (
      <div className="example">
        {modal}
        <BootstrapButton onClick={this.openModal}>Open modal</BootstrapButton>
      </div>
    );
  },
  openModal: function() {
    this.refs.modal.open();
  },
  closeModal: function() {
    this.refs.modal.close();
  }
});

// In this example we also have two components - a picture and
// a picture list. The pictures are fetched from Instagram via AJAX.


var Picture = React.createClass({

    // This component doesn't hold any state - it simply transforms
    // whatever was passed as attributes into HTML that represents a picture.

    clickHandler: function(){
        
        // When the component is clicked, trigger the onClick handler that 
        // was passed as an attribute when it was constructed:

        this.props.onClick(this.props.ref);
    },

    render: function(){

        var cls = 'picture ' + (this.props.title.split(' ').join('_') ? 'title' : '');

        return (
          <div className='col-sm-3 col-xs-6 goLeft'>
            <div className="thumbnail">
                  <div className={cls} onClick={this.clickHandler}>
                    <img src={this.props.src} width="200" title={this.props.title} className = "img-responsive portfolio-item" />
                  </div>
              <div className="caption">
                  <h3><BootstrapLabel className="label label-success">{this.props.person}</BootstrapLabel></h3>
              </div>
            </div>
          </div>
      
        );

    }

});

var PlayLink = React.createClass({
  eventHandler: function(){
      console.log("Clicked on a play event at "+this.props.ref)
      //var player_ref = document.getElementById('player');
      player_ref.seekTo(this.props.ref)
      //player_ref.play()
      //this.props.player.play()
      //console.log(this.props.tw)
      //el = document.getElementById("twbox");
      //el.innerHTML = this.props.tw;

  },
  render: function(){
      return <BootstrapButton onClick={this.eventHandler}>{this.props.ref}</BootstrapButton>;
  }
  
})

var Tweet = React.createClass({
  
  render: function(){
      return <BootstrapButton>{this.props.tw}</BootstrapButton>;
  }
  
})


var Video = React.createClass({

    // This component doesn't hold any state - it simply transforms
    // whatever was passed as attributes into HTML that represents a picture.

    clickHandler: function(){
        
        // When the component is clicked, trigger the onClick handler that 
        // was passed as an attribute when it was constructed:
        //onClick={self.pictureClick}

        this.props.onClick(this.props.ref);
    },

    eventHandler: function(){
      console.log("Clicked on a play event");

    },

    render: function(){

        var cls = 'video ' + (this.props.title.split(' ').join('_') ? 'title' : '');
        /*for(var i = 0; i < this.props.play_events[0].play_events.length; i++){
            cls_vid = 'pl_events_'+i;
            events += <div className={cls_vid} onClick={this.eventHandler}>
            {this.props.play_events[0].play_events} 
            </div>
            //this.props.play_events[0].play_events.join(',')


        }*/
        var graph_url = this.props.graph_url;
        console.log("graphurl: "+graph_url)
        var src_url = this.props.src+"?enablejsapi=1";

        var pl = function onYouTubeIframeAPIReady() {
            
            player = new YT.Player('player', {
              events: {
                'onReady': onPlayerReady,
                'onPlaybackQualityChange': onPlayerPlaybackQualityChange,
                'onStateChange': onPlayerStateChange,
                'onError': onPlayerError
              }
            });
            return player;
        }

        clsp = "video_player";
        var tag = document.createElement('script');

        tag.src = "https://www.youtube.com/iframe_api";
        var firstScriptTag = document.getElementsByTagName('script')[0];
        firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

        window.onYouTubeIframeAPIReady = function(){
          player = new YT.Player('player', {
              events: {
                'onReady': onPlayerReady
              }
          });
          console.log("Player loaded...")
          //delete window.onYouTubeIframeAPIReady;
        };
        window.onPlayerReady=function(event) {
            //event.target.playVideo();

            player_ref = event.target
            //player_ref.playVideo();
        }
        var events = this.props.items.map(function(p){
            //console.log("HERE:::::" + p.text + " " + p.play_event);
            //<BootstrapLabel className="label label-success">Tweet </BootstrapLabel>:<div id="twbox"></div>           
          
            return <PlayLink ref={p.play_event} onClick={this.eventHandler} player={pl} tw={p.text} />;   
        }).reverse();



        return (

            <div className={cls}>
                <b>{this.props.title}</b><br/>
                <iframe id="player" type="text/html" width="640" height="390" src={src_url} frameborder="0">
                </iframe><br/>
                <BootstrapLabel className="label label-success">Tracking</BootstrapLabel>: <BootstrapLabel className="label label-default">{this.props.play_events[0].person}</BootstrapLabel><br/>
                <BootstrapLabel className="label label-success">Salient point at </BootstrapLabel>:<br />{events}<br />
                <BootstrapLabel className="label label-success">Frequency graph</BootstrapLabel>:<br /><img src={graph_url} /><br />
                
            </div>

        );

    }

});

var EntriesList = React.createClass({

    getInitialState: function(){
        
        // The pictures array will be populated via AJAX, and 
        // the favorites one when the user clicks on an image:
        
        return { entries: [], favorites:[], events:[], player_ref:null};
    },

    componentDidMount: function(){
        
        // When the component loads, send a jQuery AJAX request

        var self = this;

        // API endpoint for Instagram's popular images for the day
        var dm = window.location.hostname;
        var url = 'http://'+dm+':8989/entry';

        $.getJSON(url, function(result){
            console.log(result)
            if(!result || !result.length){
                console.log("ERROR IN RETRIEVING DATA")
                return;
            }

            var entries = result.map(function(p){
                //console.log(p.img_var);
                return { 
                    date: p.date, 
                    url: p.url, 
                    description: p.description, 
                    title: p.title, 
                    image: p.image,
                    play_events: p.play_events,
                    items: p.items,
                    img_var: p.img_var

                };

            });

            // Update the component's state. This will trigger a render.
            // Note that this only updates the pictures property, and does
            // not remove the favorites array.

            self.setState({ entries:entries });

        });

    },

    pictureClick: function(url){

        // id holds the ID of the picture that was clicked.
        // Find it in the pictures array, and add it to the favorites

        //Only ever add one favorite
        this.state.favorites.length = 0;


        var favorites = this.state.favorites,
            entries = this.state.entries;

        for(var i = 0; i < entries.length; i++){

            // Find the id in the pictures array

            if(entries[i].url == url) {                  

                /*if(entries[i].favorite){
                    return this.favoriteClick(id);
                }*/

                // Add the picture to the favorites array,
                // and mark it as a favorite:

                favorites.push(entries[i]);

                break;
            }

        }
        console.log("Picture clicked!")
        // Update the state and trigger a render
        this.setState({entries:entries});

    },

    favoriteClick: function(id){

        // Find the picture in the favorites array and remove it. After this, 
        // find the picture in the pictures array and mark it as a non-favorite.

        var favorites = this.state.favorites,
            pictures = this.state.pictures;


        for(var i = 0; i < favorites.length; i++){
            if(favorites[i].id == id) break;
        }

        // Remove the picture from favorites array
        favorites.splice(i, 1);


        for(i = 0; i < pictures.length; i++){
            if(pictures[i].id == id) {
                pictures[i].favorite = false;
                break;
            }
        }

        // Update the state and trigger a render
        this.setState({pictures: pictures, favorites: favorites});

    },

    render: function() {

        var self = this;

        var entries = this.state.entries.map(function(p){
            return <Picture ref={p.url} src={p.image} title={p.title} person={p.play_events[0].person} onClick={self.pictureClick} />
        });

        if(!entries.length){
            entries = <p>Loading streams...</p>;
        }

        var favorites = this.state.favorites.map(function(p){
            return <Video ref={p.url} graph_url={p.img_var} play_events={p.play_events} items={p.items} src={p.url} title={p.title} />
        });

        if(!favorites.length){
            favorites = <p>Click an image to load the corresponding video.</p>;
        }
                

        return (
          <BootstrapPortfolio entries={entries} favorites={favorites} />
        );
    }
});


var BootstrapPortfolio = React.createClass({
  render: function(){
  return (
    <div>
    <nav className="navbar navbar-inverse navbar-fixed-top" role="navigation">
          <div className="container">
              <div className="navbar-header">
                               </div>
              <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                  <ul className="nav navbar-nav">
                      <li>
                          <a href="#">About</a>
                      </li>
                      <li>
                          <a href="#">Services</a>
                      </li>
                      <li>
                          <a href="#">Contact</a>
                      </li>
                  </ul>
              </div>
              </div>
          </nav>
    <div className="container">
    <div className="row">
            <div className="col-lg-12">
                <h1 className="page-header">Politify
                    <small>Track saliency during televised debates.</small>
                </h1>
            </div>
        </div>
        <div className="row">

        <div className="row">
            
        </div>
        <div className="row">
            
        </div>

        <hr />
        <div className="col-md-4">
                <h3>Project Description</h3>
                <p>
                Track saliency during televised events. The name of the person tracked is under the video.
                </p>
                <h3>How to search through a video.</h3>
                <ul>
                    <li>Click on the thumbnail.</li>
                    <li>The corresponding video is loaded.</li>
                    <li>Click on the buttons with seconds timestamps in the video to skip to the part where there is a salient point, given the number of tweets at that point.</li>
                    
                </ul>
                
            </div>

        <footer>
            <div className="row">
                <div className="col-lg-12">
                    <p>Politify 2015. For information tweet to @walkabout_xyz</p>
                </div>
            </div>
        </footer>

    </div>
    </div>
  );}
})

// Render the PictureList component, and add it to body.
// I am using an API key for a Instagram test ap. Please generate and 
// use your own from here http://instagram.com/developer/

React.renderComponent(
    <EntriesList />,
    document.body
);
