import os, sys
import time
lib = os.path.abspath(os.path.join(os.path.dirname( __file__ ),'api'))
sys.path.append(lib)
import datetime
from YouTube import SearchVideos
from mongo import MongoDB
from pprint import pprint
import json
import base64
from tw_grab import *
#Politify
        
class PolitifyEngine(object):
    """This class reads a set of tweets from a database, by input object, then matches it with youtube video"""

    def __init__(self, record = []):
        """Initializes PolitifyEngine class.

        :param record:
            Holds program people information objects, list.
        """
        self.record = record
        self.get_tw = Politify()
        self.dbconn = MongoDB()
        self.youtube = SearchVideos()



    def readData(self, word = "", date = "", starttime = "00:00:00"):
        date_utc = int(self.convertToUTC(date, starttime = starttime))
        entries = self.dbconn.filterTweets(word = word, date = date_utc)
        return entries


    def setVideoObject(self, prog = {}, entry = {}):
        if len(prog)==0:
            prog = self.record
        if len(entry) == 0:
            id_video = prog['url'].split('=')[1]
            entry = self.youtube.getVideo(id_video = id_video)
        
        try:
            image_file = open(prog['img_var'], 'rb')
            img_var = "data:image/jpeg;base64, %s" % base64.b64encode(image_file.read())
            image_file.close()
        except: 
            img_var = None
        return self.youtube.createVideoObject(
            entry = entry,
            cp = prog['cp'],
            date = prog['date'],
            extra_words = prog['keywords'], 
            program_title = prog['title'],
            program_description = prog['description'],
            start_time = prog['start_time'],
            img_var=img_var
        )

    # MOVE TO SEPARATE FILE
    def getAllEntries1(self, coll = "programs"):
        """Get all entries from mongodb"""
        
        entries = self.dbconn.readAllByColl(coll)

        all_videos = []
        try:
            for entry in entries:
                #print entry
                video = self.run(silent=False, rec = [entry])
                video_pop = {}
                try:
                    its = video.__dict__.items()
                except:
                    its = video.items()
                for key, val in its:
                    if not val is None:
                        try:
                            if not len(val) == 0:
                                video_pop[key]=val
                        except:
                            video_pop[key]= val
                all_videos.append(video_pop)
        except Exception as e:
            print "[PolitifyEngine][getAllEntries] Error %s" %e
        print "[PolitifyEngine][getAllEntries] Retrieved: %s entries" % len(all_videos)
        return all_videos

    def getAllEntries(self, coll="videos"):
    	from bson.json_util import loads
    	from bson.json_util import dumps
        videos = loads(dumps(self.dbconn.readAllByColl(coll)))
      	
        return videos

    def run(self, silent = False, rec = [], save = False):
        if len(rec) == 0:
            rec = self.record
        self.save = save
        if save is True:
            saved_objects = self.dbconn.addToMongo([rec], obj_type = 'program')
        if not isinstance(rec,list): print "[PolitifyEngine][run] Record input is not a list!"
        try:
            #LIMIT for now to one record:

            print "[PolitifyEngine][run] Retrieving: %s %s" % (rec[0]['keywords'][0], rec[0]['date'])
            tweets_by_day = self.readData(word = rec[0]['keywords'][0], date = rec[0]['date'])
        except Exception as e:
            print "[Error] in reading tweets: %s" % e
        try:
            video = self.setVideoObject(prog = rec[0])
        except Exception as e:
            print "[Error] in getting video object: %s" % e

        try:
            print "[run] tweets_by_day:"
            pprint(tweets_by_day)
            video_with_tweets = self.matchVideoWithStreamTime(tweets_by_day, video, input_params=rec[0])
            if silent is False:
                print "======Video with tweets======"
                pprint(vars(video_with_tweets))
            return video_with_tweets
        except Exception as e:
            print "[Error] in matching video with tweets: %s" %e

    def matchVideoWithStreamTime(self, entries, video, input_params = {}, max_tweets_in_range = 2000):
        """Associate a video with a start time in a stream of tweets. Save video dict.

        :param entries:
            Total number of tweets dicts in specific day, list.

        :param video:
            A Video.Video() object, class.

        Returns Video.Video() class object with updated items.
        """
        st = ""
        try:
            st = video.start_time
        except Exception as e:
            print "[Error][matchVideoWithStreamTime] No video start time: %s " % e
            
            #TODO calculate start time as variance of change in tweets stream
        #time range of tweets = duration of video from start time
        #convert video starttime and date to utc
        # filtered_tweets = start_time < tweets < (start_time + duration)
        ts = "%s %s" % (video.date, video.start_time)
        utc_start_time = self.convertToUTC(ts)
        tweets_after_start = []# [tweet for tweet in entries if tweet['time'] >= utc_start_time]
        
        params = {
            "start_date": ts,
            "date": {
                "time_interval": 1,
                "program_date": video.date
            },
            "min_tweets": input_params['min_tweets'],
            "keywords": [video.extra_words[0]+"_"+video.program_title]
        }
            
        try:
            tweets_by_week, tweets_in_range, tweets_by_time, full_tweets_in_program = self.get_tw.organizeTweets(
                cycle = entries,
                params = params
            )
        except Exception as e:
            print "[matchVideoWithStreamTime][ERROR] In organizeTweets: %s" % e
            exc_type, exc_value, exc_traceback = sys.exc_info()
            error = "[matchVideoWithStreamTime] Error whilst running HOST:\n\n%s" % traceback.format_exc(limit=10)
            print error
        try:
            filtered_tweets = self.get_tw.plotOverTimeByHour(tweets_in_range, params = params)
        except Exception as e:
            print "[matchVideoWithStreamTime][ERROR] Plotting: %s" % e
            exc_type, exc_value, exc_traceback = sys.exc_info()
            error = "[matchVideoWithStreamTime] Error whilst running HOST:\n\n%s" % traceback.format_exc(limit=10)
            print error
         
        utc_end_time = utc_start_time+video.duration#+(1000*60*60*2)
        print "[matchVideoWithStreamTime] tweets_in_range: %s"%len(tweets_in_range)
        print "[matchVideoWithStreamTime] full_tweets_in_program: %s"%len(full_tweets_in_program)
        try:
            # MATCH FILTERED TWEETS TIMES WITH FULL TWEETS
            tweets_in_range = self.matchTimesWithFullTweets(full_tweets_in_program, filtered_tweets, utc_end_time, strict=True, params = params)
            print "[matchVideoWithStreamTime] tweets_in_range: %s"%len(tweets_in_range)
        except Exception as e:
            print "[matchVideoWithStreamTime] Error in matching: %s" % e
            exc_type, exc_value, exc_traceback = sys.exc_info()
            error = "[matchVideoWithStreamTime][run] Error whilst running HOST:\n\n%s" % traceback.format_exc(limit=10)
            print error
         
        if len(tweets_in_range) > max_tweets_in_range:
            print "[matchVideoWithStreamTime] Running clusterTweetsInStream because tweets_in_range is larger than: %s" % max_tweets_in_range
            normalized_play = self.clusterTweetsInStream(tweets_in_range, utc_end_time)
            tweets_matching = normalized_play    
        else:
            tweets_matching = tweets_in_range    
        tweets_in_range_clean = []
        play_events = []
        for t in tweets_matching:
            tw = {}
            for key, val in t.items():
                if not "_" in key:
                    tw[key] = val
            event = tw['time'] - utc_start_time

            event_time_format = self.convertToDate(event)
            print "[matchVideoWithStreamTime] play event at: %s, %s" % (event_time_format, self.convertToDate(tw['time']))
            tw['play_event'] = event
            play_events.append(event)    
            tweets_in_range_clean.append(tw)

        try:
            print("[matchVideoWithStreamTime]Total tweets per day: %s \nTweets after start: %s \n Total tweets from start to end time:%s " % (entries.count(), len(tweets_after_start), len(tweets_in_range)))
        except Exception as e:
            print "[Error][matchVideoWithStreamTime] %s" % e    
        video.items = tweets_in_range_clean

        
        play_events_dict = {
            "person": entries.rewind()[0]['keywords'][0],
            "play_events":sorted(play_events)
        }
        video.play_events += [play_events_dict]
        video.end_time = utc_end_time
        if self.save: # Set by user in run() function
            try:
                saved_objects = self.dbconn.addToMongo([video], obj_type = 'video')
            except Exception as e:
                print "[ERROR][matchVideoWithStreamTime] Saving %s" % e
                saved_objects = 0
            print "[matchVideoWithStreamTime] Saved: %s objects in db" % saved_objects
        else:
            print "[matchVideoWithStreamTime] Saved: 0 objects in db, save is %s" % (self.save)
        return video

    def matchTimesWithFullTweets(self, after_start_full_tweet, times_tuple, utc_end_time, strict = False, params = {} ):
        #print times_tuple
        step = params['date']['time_interval']
        filtered = []
        #print "just times: %s"%just_times
        minimas = self.get_tw.getMinimas(times_tuple, params)#(a, selected_date, )
        #pprint(minimas)
        checked = []
        for tw in after_start_full_tweet:
            for x in range(0, len(minimas)-1):

                try:
                	# 
                    if (int(tw['time']) > minimas[x] or x == 0) and int(tw['time']) < minimas[x+1] and not minimas[x] in checked and int(tw['time']) < utc_end_time:
                            filtered.append(tw)
                            checked.append(minimas[x])
                    		
                except:
                    pass
        for f in filtered:
        	print "[matchTimesWithFullTweets] Play time", f['time']
        return filtered


    def clusterTweetsInStream(self, tweets_in_range, utc_end_time, cluster_range = 300, strict = 1):
        calcs = []
        normalized_play = []
        total_clusters = []
        current_cluster = []
        calcs = []
        totTweets = 0
        r = 1
        sign_changed = False
        for tw in tweets_in_range:
            currentTime = tweets_in_range[0]['time'] - (r * cluster_range)
            #print tw['time'], currentTime
            if sign_changed:
                total_clusters.append((current_cluster,totTweets))#[len(total_clusters)-1]
                totTweets = 0
                current_cluster = []
            if tw['time'] <= currentTime:
                print "[clusterTweetsInStream] %s < %s" % (tw['time'], currentTime)
                r += 1
                sign_changed = True
            else:
                print "[clusterTweetsInStream] %s > %s" % (tw['time'], currentTime)
                calcs.append(totTweets)
                sign_changed = False
            totTweets += 1
            current_cluster.append(tw)
            print "[clusterTweetsInStream] length current cluster", len(current_cluster)
            
        print totTweets, r, len(total_clusters)#, len(calcs), pprint(calcs)
        #print "Generating new cluster...last contained: %s" % total_clusters
        #tot = 0
        #for clust in total_clusters:
        #    tot += clust[1]
        #avg = tot/len(total_clusters)
        try:
            avg = reduce(lambda x, y: x + y, calcs) / len(calcs) #_tweets_per_cluster
            print "total clusters: %s :: avg_tweets_per_cluster: %s" %(len(total_clusters), avg)#_tweets_per_cluster)
            pprint(total_clusters)
            for clust in total_clusters:
                if clust[1] >= avg and clust[1] != strict:#_tweets_per_cluster:
                    clust[0][0]["cluster_length"] = clust[1] 
                    normalized_play.append(clust[0][0])            
        except Exception as e:
            print "[ERROR][matchVideoWithStreamTime] %s" % e
        print "Before %s After clusterization: %s" % (len(total_clusters),len(normalized_play))
        return normalized_play

    def convertToUTC(self, date, starttime = "00:00:00"):
        if len(date) <= 10:            
            date = "%s %s" % (date, starttime)
        utc_date = time.mktime(datetime.datetime.strptime(date, "%d-%m-%Y %H:%M:%S").timetuple())
        return utc_date

    def convertToDate(self, utcTime):
        if not type(utcTime)==int: int(utcTime)
        return datetime.datetime.fromtimestamp(
            utcTime
        ).strftime('%Y-%m-%d %H:%M:%S')
    


if __name__== "__main__":
    #try:
    #    keywords = [sys.argv[1]]
    #except:
    #    print "Provide a keyword!"
    #    sys.exit(2)
    #rec = [{
    #        "title":"QuestionTime",
    #        "date": "25-06-2015",
    #        "url":"https://www.youtube.com/watch?v=I2XgAoLF5Rg",
    #        "cp":"bbc",
    #        "start_time":"22:35:00",
    #       "keywords":["Suzanne Evans"],
    #        "description":"Location: Southampton, England. Topics include: Welfare cuts; migrants fleeing the Syrian Civil War; the European Union and the Euro currency and sugar contents in foods. Panel: Burnham, Andy. (Labour Party) Evans, Suzanne. (UKIP) Fraser, Giles. (Independent) Nelson, Fraser. (Independent) Rudd, Amber.  (Conservative Party)"
    #    }
    #   ]
    f = open('input.json','r')
    rec = json.loads(f.read())
    f.close()
    pprint(rec)
    pe = PolitifyEngine(record = rec)
    video_with_tweets = pe.run(save = True)
    output_filename = './tweets_time_by_keyword/output_data/'+rec[0]['title'] + '_' + \
        rec[0]['date'] + '_' + rec[0]['cp'] + \
        '_' + rec[0]['keywords'][0] + '.json'
    r = open(output_filename, 'w')
    r.write(json.dumps(video_with_tweets.__dict__, default=lambda o: '<not serializable>', indent=4))            
    #http://services.tivulaguida.it/api/epg/programming/events/datestart/04-07-2015%2000:00/dateend/05-07-2015%2000:00.json
    #http://services.tivulaguida.it/api/epg/channels.json
    #http://services.tivulaguida.it/api/epg/highlights.json?_=1435968097076
    #http://www.tivu.tv/epg_ajax.aspx/?json
    #http://www.tivu.tv/index-mobile-sat.aspx
    # play_events
