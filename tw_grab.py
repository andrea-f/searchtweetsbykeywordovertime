from TwitterSearch import *
import matplotlib
import datetime
import numpy as np
import os
from pylab import *
import json
import time
from pprint import pprint
from mongo import MongoDB
from time import gmtime, strftime
import traceback
import os
import sys
import StringIO
import urllib, base64
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas

CONSUMER_KEY = 'WKUdUSeMWw8PG1k1Y3HMzie5z'
CONSUMER_SECRET ='UDp7bJCrTDgMNdMA7zlYEOQju6grid1HM63iDC28x0aXcEbnHB'
OAUTH_TOKEN = '3031873572-eocLFHEZ2kxlyJ26zdFCKNNXU3fbNW3epuyD1zC'
OAUTH_TOKEN_SECRET = 'ZQbjmX8VdRVUYBUr5XiSOGZC5ZqHwr8AI6SPklYy1tYcI'
    
class Politify(object):
    """This class summarizes long form news content on TV using Twitter."""

    def __init__(self,keywords = [], params = {}, actual_request = True ):
        self.keywords = keywords
        self.params = params
        self.keyz = [""]*3
        self.dir_names = {
            "graphs":"graphs",
            "tweets": "tweets_time_by_keyword"
        }
        self.actual_request = actual_request
        self.working_dir = os.path.dirname(os.path.abspath(__file__))
        self.savetodb = MongoDB()
        self.img_var = ""

    def _getTwitterAuth(self):
        tso = TwitterSearchOrder() # create a TwitterSearchOrder object
        #puntata piazza pulita 2 marzo
        # it's about time to create a TwitterSearch object with our secret tokens
        ts = TwitterSearch(
            consumer_key = CONSUMER_KEY,
            consumer_secret = CONSUMER_SECRET,
            access_token = OAUTH_TOKEN,
            access_token_secret = OAUTH_TOKEN_SECRET
         )
        return tso,ts

    def validate(self,date_text):
        try:
            datetime.datetime.strptime(date_text, '%Y-%m-%d')
            return True
        except ValueError:
            #raise ValueError("Incorrect data format, should be YYYY-MM-DD")
            return False


    def setSearchOptions(self, tso, params={}, entities = False):
        #set_keywords(['since:2010-12-27','until:2010-12-26'])
        try:
            set_date_range = params['set_date_range']
        except:
            set_date_range = False
        try:
            if set_date_range is True:
                try:
                    search_param = []
                    isDateValid = self.validate(params['until'])
                    isToDateValid = self.validate(params['since'])
                    if isToDateValid is True:
                        param = 'since:'+params['since']
                        search_param += [param]+params['keywords']
                    if isDateValid is True:
                        until = 'until:'+ params['until']
                        search_param += params["keywords"]+[until]#.append(until)
                        
                    else:
                        print "[setSearchOptions] Invalid daterange: %s" % params['until']
                        raise
                    print "isDateValid: %s isToDateValid: %s" % (isDateValid, isToDateValid)
                    tso.set_keywords(search_param)
                except Exception as e:
                    print "[ERROR] In setting date range: %s" %e
                    tso.set_keywords(params["keywords"])
            else:        
                tso.set_keywords(params["keywords"])
            #tso.set_keywords(params["keywords"]) # let's define all words we would like to have a look for
            print "set_date_range %s" % set_date_range
        except Exception as e:
            "Please provide some keywords to search with. %s" % e
        try:    
            language = params['language']
        except Exception as e:
            "Please set a language to search with. Defaulting to 'en' "
            language = 'en'
        tso.set_language(language)
        try:
            tso.set_until(params['date'])
        except:

            pass
        try:    
            entities = params['entities']
        except Exception as e:
            pass

        print "Searching twitter with: %s %s" % (params, entities)
        
        tso.set_include_entities(entities) # and don't give us all those entity information

        return tso

    def organizeTweets(self, cycle = [], clusters_range = 0, f = None, tweets_by_cluster = [], num = 0, actual_request = False, params = {}, tweets_by_time = [], previous = 0, xticks = [], tweets_by_week = [], tweets_in_range = [], tweet_time = "", tweets_by_day_list = [], tweets_after_start = []):
        """
        :param cycle:
            Tweets retrieved either from db or live - dict, list.
        """
        num_of_tweets = 0
        full_tweets_in_program = []
        if len(params) == 0:
           params = self.params
        print "[organizeTweets] PARAMS: %s "%params
        
        if clusters_range == 0:
            try:
                clusters_range = params['clusters_range']
            except:
                clusters_range = 86400
        try:
            for tweet in cycle:
                if actual_request == True:
                    if not f is None:
                        f.write(tweet['created_at']+"\n")
                    tweet_time = tweet['created_at']
                    text = tweet['text']
                    try:
                        username = tweet['user']['screen_name']
                    except: 
                        pprint(tweet)
                    
                    time_zone = tweet['user']['time_zone']
                    location = tweet['user']['location']
                else:
                    try:
                        tweet_time = tweet.strip()
                    except:
                        tweet_time = tweet['time']
                #print "[organizeTweets] Tweet time: %s"%tweet_time
                #ts IS TIME IN STRING AS YEAR-MONTH-DAY HOUR:MINUTE:SECOND
                try:
                    try:
                        ts = time.strftime('%Y-%m-%d %H:%M:%S', time.strptime(tweet_time,'%a %b %d %H:%M:%S +0000 %Y'))#.split()
                            #tm IS TIME IN UTC
                        tm = time.mktime(datetime.datetime.strptime(ts, "%Y-%m-%d %H:%M:%S").timetuple())
                        #print "Tweet time: %s" % tm,ts
                    except:
                        tm = tweet_time
                    if previous == 0:
                        old = tm
                        previous +=1
                        xticks.append(tm)
                    diff = abs(int(old) - int(tm))
                    old = int(old)
                    tm = int(tm)
                   # print old,tm, diff
                    if diff < (clusters_range):
                        num_of_tweets +=1
                        tweets_by_day_list.append(tm)
                        try:
                            ts = "%s-%s-%s %s" % (params['date']['day'],params['date']['month'],params['date']['year'], params['date']['start_time'])
                        except:
                            ts = params['start_date']
                        #print ts
                        utc_start_time = self.convertToUTC(ts)
                        #print tm, utc_start_time
                        if tm >= utc_start_time:
                            #print "tweet is later "
                            full_tweets_in_program.append(tweet)
                            tweets_after_start.append(tm)

                    else:
                        #print "Tw date and start time: %s %s" % (self.convertToDate(tm), self.convertToDate(utc_start_time))
                        if tm >= utc_start_time:
                            tweets_in_range.append((tm, tweets_after_start))
                        elif len(tweets_in_range) == 0:
                            tweets_in_range.append((tm, tweets_after_start))
                        tweets_by_week.append((tm, tweets_by_day_list))
                        tweets_by_day_list = []
                        #if tm <= utc_start_time:
                        tweets_after_start = []
                        #tweets_by_cluster.append(num_of_tweets)
                        num_of_tweets = 0
                        num += 1
                        previous = 0
                except Exception as e:
                    print "[organizeTweets][ERROR] %s" % e                

                #tws.append(ts.split()[1])
                if actual_request:
                    tweets_by_time.append({
                        "time":tm,
                        "text": text,
                        "username": username,
                        "keywords": params['keywords'],
                        "location": location,
                        "time_zone": time_zone
                    })

        except Exception as e:
            print "[organizeTweets][ERROR] In cycling tweets: %s" % e 
            print "[ERROR] In clustering by range: %s" % e
            exc_type, exc_value, exc_traceback = sys.exc_info()
            error = "[organizeTweets] Error whilst running HOST:\n\n%s" % traceback.format_exc(limit=10)
            print error
         

        # HANDLE THE CASE WHERE ALL TWEETS ARE WITHIN A 1 DAY RANGE
        try:
            if len(tweets_by_week) == 0:
                tweets_by_week.append((tm, tweets_by_day_list))
            if len(tweets_in_range) == 0:
                tweets_in_range.append((tm, tweets_after_start))
        except Exception as e:
            print "[organizeTweets] Error in adding one record %s" % e
        #print tweets_in_range
        print "[organizeTweets] tweets_by_week: %s" % len(tweets_by_week)
        print "[organizeTweets] tweets_in_range: %s" % len(tweets_in_range[0][1])
        print "[organizeTweets] tweets_by_time: %s" % len(tweets_by_time)
        print "[organizeTweets] full_tweets_in_program: %s" % len(full_tweets_in_program)
        return tweets_by_week, tweets_in_range, tweets_by_time, full_tweets_in_program
        

    def searchTwitter(self, keywords = [], language="en", params = {}, actual_request = None, clusters_range = 86400):
        #db.entries.find({"text":{"$regex":"Evans"}})
        try:
            print actual_request
            if len(params) == 0:
                params = self.params
            print params
            if len(keywords)==0:
                keywords = self.keywords
                if len(self.keywords) == 0:
                    keywords = params['keywords']
            if actual_request is None:
                actual_request = self.actual_request
             # this is where the fun actually starts :)
            #cluster range in seconds, average TV program length 2hours
            #in seconds:
            
            #number of clusters
            try:
                tso, ts = self._getTwitterAuth()
                tso = self.setSearchOptions(tso, params)
            except Exception as e:
                print e
            #avoid querying Twitter every time
            cycle = []
            fn = self.working_dir+'/'+self.dir_names['tweets']+'/tweets_by_time_'+params['keywords'][0]
            print "actual_request: %s"%actual_request, fn
            if actual_request == True:
                f = open(fn,'w')
                #
                try:
                    cycle = ts.search_tweets_iterable(tso)
                except Exception as e:
                    print e                
            else:
                f = open(fn,'r')
                for c in f:
                    cycle.append(c)
                print "[searchTwitter] Total tweets read from local storage: %s"%len(cycle)
            try:
                tweets_by_week, tweets_in_range, tweets_by_time, full_tweets_in_program = self.organizeTweets(cycle = cycle, f = f, actual_request = actual_request)
            except Exception as e:
                print "[searchTwitter][ERROR] In organizeTweets: %s" % e
            
        except TwitterSearchException as e: # take care of all those ugly errors if there are some
            print("[searchTwitter][ERROR] In main searchTwitter function: %s "%e)
        
        #print "tweets_by_week: %s"%tweets_by_week
        #print "tweets_in_range: %s"% tweets_in_range

        try:
            first = self.convertToDate(tweets_by_week[0][0])
            last = self.convertToDate(tweets_by_week[len(tweets_by_week)-1][0])
        except Exception as e:
            print "[ERROR][searchTwitter] %s + tweets_by_week: %s" % (e,tweets_by_week)
            first = ""
            last = ""
        self.keyz[1]=last
        self.keyz[0]=first
        self.keyz[2]=keywords[0]

        #filter tweets by program time range, list of tuple of time of first tweet and all tweets within that range


        #tweets_by_program_time = 
        return {
            "tweets_by_week": tweets_by_week,
            "tweets_in_range": tweets_in_range, 
            "tweets_by_time": tweets_by_time
        }

    def plotTweetsTimeByProgramRange(self, tweets):
        """Selects tweets by time within program range.

        :param tweets:
            tuple of time of first tweet and all tweets within that range, list.

        """
        params = self.params
        ts = "%s %s" % (params['since'], params['start_time'])
        utc_start_time = self.convertToUTC(ts)
        tweets_after_start = [tweet for tweet in tweets if tweet >= utc_start_time]
        # duration is in seconds?
        #utc_end_time = utc_start_time+video.duration
        #tweets_in_range = [tweet for tweet in tweets_after_start if tweet['time'] <= utc_end_time]
        if len(keyz) == 0:
            keyz = self.keyz
        if len(params) == 0:
            params = self.params
        y1 = [len(tw_tuple[1]) for tw_tuple in tweets_by_week]
        #print y1
        
        days = [(lambda x: x[0])(tw_tuple) for tw_tuple in tweets_by_week]
        tweets_by_week_keywords = []
        for t in tweets_by_week:
            tweets_by_week_keywords.append((t[0],t[1],params['keywords']))
        print days, keyz #selected_date = ymd
        self.filterTweetsByCustomTimeInterval(tweets_after_start, selected_date = selected_date, time_interval = 30)
        #self.savetodb.addToMongo(tweets_by_week_keywords)
        selected_date = keyz[1] + " to " + keyz[0]
        dates_str = [self.convertToDate(d).split()[0] for d in days]
        self.drawFigure(days, y1, xticks_list = dates_str, keys = keyz, selected_date = selected_date, time_interval = None )

    def getMinimas(self, b, params = {'min_tweets':0}):
        maxm = []
        x = [a[1] for a in b]
        
        print "[getMinimas] number of tweets in cluster", x
        for c in range(0, len(x)-1):
            if c > 0:
                print "[getMinimas] play event, number of tweets", c, x[c]
                if x[c] >= x[c-1] and x[c] >= x[c+1]:
                    if x[c] >= params['min_tweets']:
                        maxm.append(b[c][0])
        #print maxm
        #from scipy.signal import argrelextrema

        print "[getMinimas] len max",len(maxm)
        # for local maxima
        #x1 = np.array(x)
        #maxm2 = argrelextrema(x1, np.greater)

        # 1for local minima
        #argrelextrema(x, np.less)
        print "[getMinimas] maxm", maxm
        #print maxm2
        return maxm

    def clusterTweetsByTimeRange(self, cycle, clusters_range = 86400):
        """Filter a list of tweets dicts by a specific time range."""
        clusters = []
        previous = 0
        tweets = []
        tweets_by_cluster = []
        tweets_by_cluster.append(0)
        tweets_by_time = []
        tws=[]
        num = 0
        xticks = []
        num_of_tweets = 0
        tweets_by_day = 0
        tweets_by_day_list = []
        tweets_by_week = []
        try:
            for tweet in cycle:
                #print tweet['time']
                try:
                    ts = time.strftime('%Y-%m-%d %H:%M:%S', time.strptime(tweet["time"],'%a %b %d %H:%M:%S +0000 %Y'))#.split()
                #tm IS TIME IN UTC
                    tm = time.mktime(datetime.datetime.strptime(ts, "%Y-%m-%d %H:%M:%S").timetuple())
                except:
                    tm = tweet['time']
                    ts = 0
                if previous == 0:
                    old = tm
                    previous +=1
                    xticks.append(tm)
                diff = abs(int(old) - int(tm))
                old = int(old)
                tm = int(tm)
                #print old,tm, diff
                if diff < (clusters_range):
                    num_of_tweets +=1
                    tweets_by_day_list.append(tm)
                else:
                    tweets_by_week.append((tm, tweets_by_day_list))
                    tweets_by_day_list = []
                    tweets_by_cluster.append(num_of_tweets)
                    num_of_tweets = 0
                    num += 1
                    previous = 0
                    #print "new clusters number: %s" % num
            return {
                "tweets_by_week": tweets_by_week,
                "utc_tweet_time": tm,
                "date_tweet_time": ts,
                "tweets_by_time": tweets_by_time
            }
        except Exception as e:
            print "[ERROR] In clustering by range: %s" % e
            exc_type, exc_value, exc_traceback = sys.exc_info()
            error = "{ DINX } [run] Error whilst running HOST:\n\n%s" % traceback.format_exc(limit=10)
            print error
         
            return {}

    def saveTweetsAndReport(self, tweets_by_week, tweets_by_time, clusters_range = 86400):
        """Plots different views on the same data. Either per week or per custom minute interval.

        :param tweets_by_week:
            Tuples containing (day, tweets in day utc), list.

        :param tweets_by_time:
            dict of Text, Username, Time, tweets, list.
        """
            #ms ina day : 86400000
        if self.actual_request is True:
            tit = tweets_by_time[0]['keywords'][0]
        else: 
            tit = '-'.join(self.keywords)

        t = open(self.working_dir+"/"+self.dir_names['tweets']+"/"+tit+"_total", 'a+r')
            #for tweet in tweets_by_time:
         #or tweet in tweets_by_time:
            #    if not tweet['time'] in :
        total = 0
        try:
            for tweet in tweets_by_week:
                total += len(tweet[1])
        except:
            pass
        print "Output folder: %s " % tit
        first = self.keyz[1]
        last = self.keyz[0]
        #print "Saving a total of tweets_by_time %s" % len(tweets_by_time)
        self.savetodb.addToMongo(tweets_by_time)
            #x = np.array(values)
            #set_xlim(int(args.avg_window), right=1)
        self.report({
            "last twitter retrieved": last,
            "first retrieved": first,                
            "total_tweets": total,
            "clusters length in seconds": clusters_range,
            "keywords": self.keyz[2]
        })
            
        

    def merge_dicts(self, *dict_args):
        '''
        Given any number of dicts, shallow copy and merge into a new dict,
        precedence goes to key value pairs in latter dicts.
        '''
        result = {}
        for dictionary in dict_args:
            result.update(dictionary)
        return result

    def report(self, stats):
        for key, val in stats.iteritems():
            print str(key) + ": " + str(val)


    def create_hours_minute(self, dt, time_interval = 30, zoom_in = True):
        """This function creates x label for time intervals.

        :param dt:
            Date to separate in minutes, string.

        :param time_interval:
            How much space between each tick.
        """
        hours = []
        x = 0
        
        while x <= 23:
            if zoom_in:
                minute = 0
                while minute <=59:
                    if minute < 10:
                        min_str = "0"+str(minute)
                    else:
                        min_str = str(minute)

                    hours.append(dt+" "+str(x)+":"+min_str+":00")
                    minute += time_interval
            else:
                hours.append(dt+" "+str(x)+":00:00")
            x+=1    
        try:
            hours_utc = [int(time.mktime(datetime.datetime.strptime(h, "%Y-%m-%d %H:%M:%S").timetuple())) for h in hours]
        except:
            hours_utc = [int(time.mktime(datetime.datetime.strptime(h, "%d-%m-%Y %H:%M:%S").timetuple())) for h in hours]
    
        return hours_utc

    def plotOverDays(self, tweets_by_week, keyz= [], params = {}):
        if len(keyz) == 0:
            keyz = self.keyz
        if len(params) == 0:
            params = self.params
        y1 = [len(tw_tuple[1]) for tw_tuple in tweets_by_week]
        #print y1
        days = [(lambda x: x[0])(tw_tuple) for tw_tuple in tweets_by_week]
        tweets_by_week_keywords = []
        for t in tweets_by_week:
            tweets_by_week_keywords.append((t[0],t[1],params['keywords']))
        print days, keyz
        self.savetodb.addToMongo(tweets_by_week_keywords)
        selected_date = keyz[1] + " to " + keyz[0]
        dates_str = [self.convertToDate(d).split()[0] for d in days]
        self.drawFigure(days, y1, xticks_list = dates_str, keys = keyz, selected_date = selected_date, time_interval = None )

    def plotOverTimeByHour(self, tweets_by_week, bins = 24, keyz=[], params = {}):
        """
        This function plots tweet with a keyword overtime by hour.
        :param y:
            (Required) has list of tweets time in ms from 1970,1,1, list of tuples.

        :param x:
            (Optional) x-ticks of graph

        :param xtick:
            Labels of x ticks, list.

        :param title:
            Description of graph, string.

        :param bins:
            Number of clusters to plot dates around, int.

        :param keyz:
            Date range and keyword used for search, tuple.

        :param params:
            Input to search options, list.

        """
        #xticks, keyz = [first,last,keywords[0]], params = params
        if len(params) == 0:
            params = self.params
        if len(keyz)==0:
            keyz=self.keyz
        try:
            try:
                y = params['date']['year']
                d = params['date']['day']
                m = params['date']['month']
                if d<10:
                    dd = "0"+str(d)
                else:
                    dd = str(d)
                if m<10:
                    mm = "0"+str(m)
                else:
                    mm = str(m)
                selected_date = str(y)+"-"+str(mm)+"-"+str(dd)
            except:
                selected_date = datetime.datetime.strptime(params['date']['program_date'], '%d-%m-%Y').strftime('%Y-%m-%d')
            
            try:
                    time_interval = params['date']['time_interval']
            except:
                    time_interval = 59
            try:
                min_tweets = params['min_tweets']
            except:
                min_tweets = 0            
            print "selected date: " + selected_date
            #print "tws by week : %s" % str(tweets_by_week)
            try:
                for t in tweets_by_week:
                    for tt in t[1]:
                        #print tt
                        tweets_by_selected_date.append(tt)
            except:    
                    tweets_by_selected_date = self.filterTweetsByDay(tweets_by_week, selected_date)
        except Exception as e:
            print "[plotOverTimeByHour] ERROR: %s"%e
            pass
        if len(tweets_by_selected_date)==0:
            print "[Warning] tweets_by_selected_date has len 0."
        num_of_tweets_per_interval = self.filterTweetsByCustomTimeInterval(
            tweets_by_selected_date,
            selected_date = selected_date,
            time_interval = time_interval
        )
        ### BY MINUTE
        utc_intervals = self.create_hours_minute(selected_date, time_interval)
        lst_tweets_by_interval = [x[1] for x in num_of_tweets_per_interval]
        y1 = np.array(lst_tweets_by_interval)
        x1 = np.array(utc_intervals)
        #print len(x1), len(y1)
        #print type(x1), type(y)
        hrs = [self.convertToDate(h).split()[1] for h in utc_intervals]
        #self.drawFigure(x1, y1, hrs, keyz, selected_date, time_interval)
        img_var = self.showRelevantTimes(x1, y1, min_tweets = min_tweets, selected_date = selected_date, params = params)
        tweet_times_with_intervals = [x for x in num_of_tweets_per_interval if x[1] >= min_tweets]
        #print "[plotOverTimeByHour] tweet_times_with_intervals: %s"%tweet_times_with_intervals
        return tweet_times_with_intervals
        
    def showRelevantTimes(self, x1, y1, min_tweets = 0, selected_date = "", params = {}):
        z = zip(x1,y1)
        s = filter((lambda x: x[1]>min_tweets) ,z)
        x2 = [item[0] for item in s]
        y2 = [item[1] for item in s]
        try:
            self.savetodb.addToMongo({
                "zip_tweets_frequency":z,
                "min_tweets_frequency":s,
                "keywords": self.keywords
            })
        except Exception as e:
            print "[showRelevantTimes][ERROR] %s" % e
        one_day_in_ms = 86400000
        keyz = self.keyz
        if len(selected_date) == 0:
            selected_date = "%s-%s-%s"%(params['date']['year'],params['date']['month'],params['date']['day'])
        new_time_interval = int((((one_day_in_ms/len(s))/1000)/60))
        hrs2 = [self.convertToDate(h).split()[1] for h in x2]
        
        try:
            if len(keyz[2]) == 0:
                keyz[1] = str(params['min_tweets'])
                keyz[2] = params['keywords'][0].replace(" ","_")
        except Exception as e:
            print "[showRelevantTimes][ERROR] %s" % e
            pass
        img_var = self.drawFigure(x2, y2, hrs2, keyz, selected_date, new_time_interval)
        #plt.hist(y, len(x)-1, color="#F08080", alpha=.5)
        return img_var

    def drawFigure(self, x,y, xticks_list = [], keys = [], selected_date = "", time_interval = 30):
        try:
            fig = plt.figure()
            plt.plot(x,y)
            xlabel('time (hours)')
            #print x
            xticks(x, xticks_list, rotation='vertical')
            #xticks(self.create_hours_minute(selected_date, time_interval), xticks, rotation='vertical')
            fig.set_size_inches(18.5, 10.5) 
            #xticks( x, [self.convertToDate(hour).split()[0].replace('2015-','') for hour in tweets_by_selected_date] )  
            ylabel('tweets')
            #plt.title('Tweets from %s for: %s' % (selected_date, str(keys[2].replace("_"," "))))
            plt.title('Tweets')
	    #time_now = strftime("%Y-%m-%d_%H:%M:%S", gmtime())
            path = "/"+self.dir_names['graphs']+"/"+str(keys[2])+"_"+str(selected_date)+"_"+str(time_interval)+"ms_"+keys[1]+"min.png"
            output_name = self.working_dir+"/webapp"+path
            print "[drawFigure] absolute: %s"%output_name
            print "[drawFigure] relative: %s"%path
            fig.tight_layout()
            fig.savefig(output_name)
            img_var = self.convertFigToHtml(fig)
            ###### SET CLASS VARIABLE WITH IMG STRING IN BASE 64 FOR EASIER RETRIEVAL
            self.img_var = path
            plt.show()
            plt.close()
            # Update params
            self.update_input_object(output_name)
            return path

        except Exception as e:
            print "Error in plotting: %s"%e


    def update_input_object(self, file_location, default_config_location = "input.json"):
        """
        Saves the output location of the generated image in the input.json object.
        """
        f = open(default_config_location, 'r')
        json_data = json.loads(f.read())
        json_data[0]['img_var'] = file_location
        f.close()
        n = open(default_config_location, 'w')
        n.write(json.dumps(json_data, indent=4))
        n.close()

    
    def convertImageToBase64(self,):
        "Convert image to base64 so it can be stored in a variable"    
        try:
            imgdata = StringIO.StringIO()
            fig.savefig(imgdata, format='png')
            imgdata.seek(0)  # rewind the data

            print "Content-type: image/png\n"
            uri = 'data:image/png;base64,' + urllib.quote(base64.b64encode(imgdata.buf))
            print '<img src = "%s"/>' % uri
        except Exception as e:
            print "[convertImageToBase64][ERROR] %s" % e
            uri = ""
        return uri

    def convertFigToHtml(self, fig):
        """
        
        Convert Matplotlib figure 'fig' into a <img> tag for HTML use using base64 encoding.
        https://gist.github.com/mjanv/90d4ef43edbff3f3b0fc
        
        """
        try:
            canvas = FigureCanvas(fig)
            png_output = StringIO.StringIO()
            canvas.print_png(png_output)
            data = png_output.getvalue().encode('base64')
            return urllib.quote(data.rstrip('\n'))
        except Exception as e:
            print "[convertFigToHtml] ERROR %s"%s
            return ""
        #return '<img src="data:image/png;base64,{}">'.format(urllib.quote(data.rstrip('\n')))
        

    def filterTweetsByDay(self, tweets, selected_date):
        """This function filters tweet by a specific date.

        :param tweets:
            contains tuples of (day, tweets_by_time_utc), list.

        :param selected_date: 
            Month-day "MM-DD", string.

        Return list with tweets by selected_date.
        """
        tweets_by_selected_date = []
        #tweets by day is a tuple of (day, tweets times that date)
        for tweets_by_day in tweets:
            day = tweets_by_day[0]
            tweets_in_day = tweets_by_day[1]
            #print day, tweets_in_day
            #print tweets
            try:
                #tweets_by_selected_date = [tweet for tweet in tweets if selected_date in self.convertToDate(tweet).split()[1]]
                for tweet_utc in tweets_in_day:
                    tweet_hms = self.convertToDate(tweet_utc)
                    #print tweet_hms, selected_date
                    if selected_date in tweet_hms:
                        #if len(dt) == 0:
                        #    dt = tweet_hms.split()[0]
                        tweets_by_selected_date.append(tweet_utc)

            except Exception as e:
                print e
                tweets_by_selected_date = []
        #print len([day[0] for day in x]), len(y)
        #get by hour
        return tweets_by_selected_date

    def filterTweetsByCustomTimeInterval(self, tweets_by_selected_date, selected_date = "", time_interval = 30):
        if len(selected_date) == 0:
            print "[Warning] Please provide a selected_date to filterTweetsByCustomTimeInterval."
        utc_time_intervals = self.create_hours_minute(selected_date, time_interval)
        tweet_by_interval = []
        num_of_tweets_per_interval = []
        #print utc_time_intervals
        #print tweets_by_selected_date
        for a in range(0, len(utc_time_intervals)):
            z = 0
            for tw in tweets_by_selected_date:
                try:
                    if tw >= utc_time_intervals[a] and tw <= utc_time_intervals[a+1]:
                        z +=1
                        #if tw not in utc_time_intervals: 
                            #utc_time_intervals.append(tw)
                except: pass
            num_of_tweets_per_interval.append((utc_time_intervals[a], z))

        return num_of_tweets_per_interval
        
    def filterTweetsByHour(self,  tweets_by_selected_date):
        tweets_by_hour = []
        tweets_per_hour = []
        currentHour = "00"

        for tw in tweets_by_selected_date:
            hr = self.convertToDate(tw).split()[1].split(":")[0]
            if hr in currentHour:
                tweets_by_hour.append(tw)
            else: 
                tweets_per_hour.append((int(hr), len(tweets_by_hour)))
                currentHour = hr
                tweets_by_hour = []
        #print tweets_per_hour
        ordered_tweets_by_hour=sorted(tweets_per_hour,key=lambda x: x[0], reverse = False)
        full_hours = [0]*24
        print "ordered_tweets_by_hour:: %s"%ordered_tweets_by_hour
        x = 0
        for tw in ordered_tweets_by_hour:

            full_hours[tw[0]] = tw[1]
            if x == 23:
                break
            
        return full_hours


    def convertToDate(self, utcTime):
        if not type(utcTime)==int: int(utcTime)
        try:
            return datetime.datetime.fromtimestamp(
                    utcTime
            ).strftime('%Y-%m-%d %H:%M:%S')
        except:
            return datetime.datetime.fromtimestamp(
                    utcTime
            ).strftime('%d-%m-%Y %H:%M:%S')

    def convertToUTC(self, date, starttime = "00:00:00"):
        if len(date) <= 10:         
            date = "%s %s" % (date, starttime)
        try:
            utc_date = time.mktime(datetime.datetime.strptime(date, "%d-%m-%Y %H:%M:%S").timetuple())
        except:
            utc_date = time.mktime(datetime.datetime.strptime(date, "%Y-%m-%d %H:%M:%S").timetuple())    
        return utc_date




if __name__== "__main__":
    f = open('input.json', 'r')
    rec = json.loads(f.read())[0]
    f.close()
    program_date = time.mktime(
        datetime.datetime.strptime(rec['date'], "%d-%m-%Y").timetuple())
    day = 86400 # in ms
    since = program_date - day
    until = program_date + day
    until_date = datetime.datetime.fromtimestamp(until)
    until_date = until_date.strftime('%Y-%m-%d')
    since_date = datetime.datetime.fromtimestamp(since)
    since_date = since_date.strftime('%Y-%m-%d')
    date_split = rec['date'].split('-')
    day = date_split[0]
    month = date_split[1]
    year = date_split[2]
    params = {
        "keywords" : rec['keywords'],
        "language" : rec['language'],
        "min_tweets":rec['min_tweets'],
        "set_date_range":True,
        "until":until_date,
        "since":since_date,
        "date": {
            "day":day,
            "month": month,
            "year": year,
            "start_time": rec['start_time'],
            "end_time": rec['end_time'],
            "time_interval": 1
        }
        #"date":1425341144
    }
    # NEED TUNER FOR:
    # cluster size
    # minimum number of tweets per cluster
    # number of bins
    # add delay
    pprint(params)
    st = Politify(params = params)
    tweets = st.searchTwitter(actual_request = True)
    
    tweets_by_day = tweets["tweets_by_week"]
    tweets_in_range = tweets['tweets_in_range']
    tweets_by_time = tweets['tweets_by_time']
    st.saveTweetsAndReport(tweets_in_range, tweets_by_time)
    #st.plotOverDays(tweets_by_day)
    #st.filterTweetsByCustomTimeInterval(tweets_by_time, selected_date = "2015-10-03", time_interval = 30)
    #print tweets_in_range
    st.plotOverTimeByHour(tweets_in_range)
    #print "first: %s" % (st.convertToDate(tweets_in_range[1][0]))

     
                
            
