import json
import urllib
import os,sys
lib = os.path.abspath(os.path.join(os.path.dirname( __file__ )))
class Freebase(object):
    """This class interacts with the freebase api."""

    def __init__(self):
        """Initializes freebase class."""


    def get_apikey(self):
        """Returns api key for query."""
        api_key = open(lib+"/freebase_api_key").read()
        return api_key
    
    def makeQuery(self, text, api_key = "", lang = "", filter = ""):
        service_url = 'https://www.googleapis.com/freebase/v1/search'
        if len(lang) == 0:
            lang = "en,it"
        if len(api_key) == 0:
            api_key = self.get_apikey()
        params = {
                'query': text,
                'key': api_key,
                'lang': lang
        }
        if len(filter) > 0:
            params["filter"] = filter
        url = service_url + '?' + urllib.urlencode(params)
        return url
    
    def search(self, query = 'blue bottle', language = "", filter = ""):
        #print "Searching freebase for: %s in %s" % (query, language)
        #, lang = language
        #alias:"+query+" 
        if "person" in filter:
            filter = "(all name:"+query+" type:/people/person)"
        url = self.makeQuery(query, self.get_apikey(), filter = filter, lang = language)
        response = json.loads(urllib.urlopen(url).read())
        #for result in response['result']:
         #   print result['name'] + ' (' + str(result['score']) + ')'
        return response['result']

    
