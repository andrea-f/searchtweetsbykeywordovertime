from apiclient import *
from apiclient.discovery import build
from apiclient.errors import HttpError
from oauth2client.tools import argparser
from pprint import pprint

DEVELOPER_KEY = "AIzaSyBmZbH7xeklHFjsvuH-4wQXOvfm0_Ynknk"
YOUTUBE_API_SERVICE_NAME = "youtube"
YOUTUBE_API_VERSION = "v3"

# YOUTUBE API CLIENT https://developers.google.com/youtube/v3/docs/videos/list

class SearchVideos():
    """This class searches videos based on an event."""


    def __init__(self, event = None):
        """Does nothing for now.
        
        :param event:
            From :class:`Event`, object.            
        """
        self.event = event

    def _setUpYTApi(self):
        """Sets up YouTube API client.

        Return YouTube API object.
        """
        youtube = build(YOUTUBE_API_SERVICE_NAME, YOUTUBE_API_VERSION,
        developerKey=DEVELOPER_KEY)

        return youtube




    def _setUpYTQuery(self, query = "", startIndex = 1, maxResults = 50):
        """Sets up YouTube Query object.

        :param query:
            Which query to run on youtube, string.

        Return YouTube Query object.
        """
        argparser.add_argument("--q", help="Search term", default="ALS Ice Bucket Challenge")
        #change the default to the search term you want to search
        argparser.add_argument("--max-results", help="Max results", default=25)
        #default number of results which are returned. It can vary from 0 - 100
        args = argparser.parse_args()
        options = args
        return options

    def getVideoInfo(self, videos_list):
        videos = []
        for yt_vid in videos_list:
            video = self.createVideoObject(yt_vid)
            videos.append(video)
        return videos

    def runYouTubeQuery(self, query, pages = 0, maxResults = 50):
        """Retrieves video feed based on query build on YouTube service.

        :param query:
            YouTube Query class item, object.

        Return list with video feed.
        """
        ytService = self._setUpYTApi()
        page = 0
        index = 1
        f = []
        while page <= pages:
            if pages==0:
                queryObject = self._setUpYTQuery(query = query)
            else:
                queryObject = self._setUpYTQuery(query = query, startIndex = index)
                index += maxResults                
            try:
                feed = ytService.search().list(
                    q = query,
                    part = "id,snippet",
                    maxResults = maxResults
                ).execute()
            except Exception as e:
                print "[YouTube][runYouTubeQuery] Error: %s" % e
        return feed.get("items", [])

    def getVideo(self, id_video = "2NxVMcf6TFc"):
        """Get YouTube video by id.
        
        :param id:
           11-chars matching specific YouTube video ID, string.
 
        Return :class:`gdata.youtube` object with video entry.
        """
        ytService = self._setUpYTApi()

        videos_list_response = ytService.videos().list(
            id=id_video,
            part='id,statistics,contentDetails,snippet,recordingDetails,player'
        ).execute()
        return videos_list_response['items'][0]
        #res = []
        #for i in videos_list_response['items']:
        #   pprint(i)
        #   temp_res = dict(v_id = i['id'])#, v_title = videos[i['id']])
        #   temp_res.update(i['statistics'])
        #   res.append(temp_res)
        #return res

    def convertDuration(self, duration):
        """PT1H21M46S"""
        import re
        try:
            p = re.compile('(\d+\w)')
            dur = p.findall(duration)
            dur_in_sec = 0
            hr = 0
            mt = 0
            sec = 0
            for t in dur:
                if "H" in t:
                    hr = int(t.replace('H','')) * 60 * 60
                elif "M" in t:
                    mt = int(t.replace('M','')) * 60
                elif "S" in t:
                    sec = int(t.replace('S',''))
            dur_in_sec = hr + mt + sec
            
        except Exception as e:
            print "[convertDuration] Error: %s" % e
            dur_in_sec = 0
        return dur_in_sec


        
    def createVideoObject(self, start_time = "", img_var = "", entry = {},program_title = "",extra_words = [], program_description="", event = {}, items_name = [], q = "Query which got the video", date = "", eventName = "",cp = "", category = ""):
        """Instantiate :class:`Video` objects.

        :param entry:
            YouTube video information, object.

        :param event:
            It is :class:`Event` instance, object.

        :param q:
            Query which got the video, string.

        Return :class:`Video` object.
        """
        import Video
        yt_prefix = 'http://www.youtube.com/embed/'
        video = Video.Video()
        #pprint(entry)
        try:
            video.title = entry['snippet']['title']
        except:
            pass
        try:
            video.program_title = program_title
        except:
            pass
        try:
            video.category = entry.media.category[0].label
        except:
            pass
        try:
            video.query = q
        except:
            pass
        try:
            video.channel_id = entry['snippet']['channelId']
        except:
            pass
        try:
            video.url = yt_prefix+entry['id']
        except:
            pass

        try:
            video.start_time = start_time
        except:
            pass

        try:        
            dur = entry['contentDetails']['duration']
            self.convertDuration(dur)
            #print hours, minutes, secs
            video.duration = self.convertDuration(dur)
            #print video.duration
        except Exception as e:
            print "[createVideoObject] Couldnt extract time: %s" % e
            pass
        try:
            video.event_name = eventName
        except:
            pass
        try:
            video.image = entry['snippet']['thumbnails']['high']['url']
        except:
            pass
        try:
            video.channel_title = entry['snippet']['channelTitle']
        except:
            pass
        try:
            video.cp = cp
        except:
            pass
        try:
            video.category_of_event = category
        except:
            pass
        try:
            video.upload_date = entry['snippet']['publishedAt']
        except:
            pass
        try:
            video.uploader = entry.author[0].name.text
        except:
            pass
        try:
            video.extra_words = extra_words
        except:
            pass
        try:
            video.items = items_name
        except:
            pass
        try:
            video.program_description = program_description
        except:
            pass

        try:
            video.img_var = img_var
        except:
            pass
        try:
            video.viewcount = entry['statistics']['viewCount']
        except:
            pass
        try:
            video.description = entry['snippet']['description']
        except:
            pass
        try:
            video.keywords= entry.media.keywords.text
        except:
            pass
        try:
            if len(date) == 0:
                video.date = queries[0].split()[0]
            else:
                video.date = date
        except:
            video.date = date
        video.play_events = []
        return video