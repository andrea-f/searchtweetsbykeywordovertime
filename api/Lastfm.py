lib = os.path.abspath(os.path.join(os.path.dirname( __file__ ), '..', '..','ConfigLibrary'))
sys.path.append(lib)
import pylast

class LastFM():
    """Uses LastFm db for matching found songs."""
    
    def __init__(self):
        """Initializes lastfm service."""
        self.network = self.getNetworkInstance()
    
    def _getAPIKeys(self):
        """Returns LastFM API and API SECRET."""
        return {
            "API_KEY" : "897dac8d547aa8ca191a08a832fb4b7f",
            "API_SECRET" : "2085628097ebb522c8521966eaccbb92"
        }


    def getUserNamePwd(self):
        """Returns LastFM username and password."""
        username = "your_user_name"
        password_hash = pylast.md5("your_password")
        return {
            "username": username,
            "pwd": password_hash
        }
    
    def getNetworkInstance(self):
        """Returns LastFM network instance."""
        apiInfo = self._getAPIKeys()
        network = pylast.LastFMNetwork(api_key = apiInfo['API_KEY'], api_secret = apiInfo['API_SECRET'])
        return network
        

    def getTrack(self, artist = "", track = ""):
        trackInfo = self.network.search_for_track(artist, track)
        return trackInfo

    def getArtist(self, artist):
        artist = self.network.search_for_artist(artist)
        return artist
    
    