#ffmpeg -i <input-file> -ss <time> -vframes 1 file%d.jpg

import subprocess
import shlex
import os, sys
import time
lib = os.path.abspath(os.path.join(os.path.dirname( __file__ ),'..'))
sys.path.append(lib)
import datetime
from mongo import MongoDB
import pprint
import base64
import urllib2, json
from subprocess import check_output
from termcolor import colored
import ast
from bson.json_util import loads
from bson.json_util import dumps

class AnalyseVideo:

	def __init__(self):
		"""Match timecodes extracted from Twitter of TV program person speaking with multiple known manually annotated shots of the person to match accuracy of the timecode"""
		self.dbconn = MongoDB()
        


	def extractFrames(self, timecodes = [], video_file_location = "", frames_folder = "", frame_name = ""):
		"""Opens video with ffmpeg and extracts frames at specific times"""
		frames_ref = []

		print "[extractFrames] from %s" % video_file_location
		if len(frames_folder)==0:
			frames_folder = os.path.abspath(os.path.join(os.path.dirname( __file__ ))) + ""
			#frames_folder = video_file_location.split('/')
			#frames_folder = frames_folder[len(frames_folder)-1]
		try:    
			for timecode in timecodes:   

				full_frame_name = "%s/%s_%s.jpg" % (frames_folder, frame_name.replace("/","-").replace("\"","").replace(' ', '_'), timecode)
				print "[extractFrames] Frame name: %s" % full_frame_name
				frames_ref.append(full_frame_name)
				if not os.path.exists(full_frame_name):
					print "[extractFrames] Extracting frame..."
					command = 'ffmpeg -i %s -ss %s -vframes 1 %s' % (video_file_location, timecode, full_frame_name)     # your command goes here
					subprocess.call(shlex.split(command))
		except Exception as e:
			print "[extractFrames][ERROR] In extracting frame: %s" % e
		return frames_ref

	def getVideo(self, ref = "", file_name = ""):
		"""Get video from service."""
		if not os.path.isfile(file_name):
			command = 'youtube-dl %s -o %s' % (ref, file_name)     # your command goes here
			print "[getVideo] Running command: %s " % (command)
			subprocess.call(shlex.split(command))
		else:
			print "[getVideo] %s already exists." % (file_name)
		return file_name

	def readImages(self, files_location = ""):
		"""Get images from filesystem."""
		files = os.listdir(files_location)
		images = [files_location+"/"+image_location for image_location in files if image_location.endswith(".jpg") or image_location.endswith(".png")]
		return images

	def runAlgorithmiaCommand(self, img):
		"""Perform authenticated operations with Algorithmia API."""
		url = "https://api.algorithmia.com/v1/data/andreaf/images"
		command = "curl -X PUT -F file=@%s -H 'Authorization: Simple %s' %s" % (img, self.getAlgorithmiaAPIKey(),url)     # your command goes here
		#print command
		#subprocess.call(shlex.split(command))
		#out = check_output([command])
		cmd = subprocess.Popen(command, stdout=subprocess.PIPE, shell=True)
		cmd_out, cmd_err = cmd.communicate()
		#Algorithmia.file(uri).putFile(img)
		#curl -X PUT -F file=@filename.csv -H 'Authorization: Simple simSVo7vxBvRDhxW+Xgb7WOpBdT1' https://api.algorithmia.com/v1/data/andreaf/newCollection
		#subprocess.call(shlex.split(command))
		print cmd_out
		try:
			back = ast.literal_eval(cmd_out)['result'][0]
		except Exception as e:
			print e
			back = cmd_out
		return back


	def getAlgorithmiaAPIKey(self):
		return "simSVo7vxBvRDhxW+Xgb7WOpBdT1"

	def compareImageWithScreenshot(self, image1, image2):
		"""Compares two images based on Algorithmia API."""
		import Algorithmia
		#input = ["data://zskurultay/ImageDemo/butterfly1.png","data://zskurultay/ImageDemo/butterfly1.png"]
		images = [image1, image2]
		print "Calling Algorithmia..."
		client = Algorithmia.client('simSVo7vxBvRDhxW+Xgb7WOpBdT1')
		input = []
		encoded_images = []
		for img in images:
			uri = "data://andreaf/images/"+img.split('/')[-1]
			#data://.algo/:author/:algoname/temp/:filename
			uri_full = "https://algorithmia.com/v1/data/andreaf/images/"+img.split('/')[-1]
			#Algorithmia.file(uri).putFile(img)
			urlExists = Algorithmia.file(uri_full).exists()
			print uri_full, urlExists
			fileExists = Algorithmia.file(uri).exists()
			if fileExists:
				print colored("[compareImageWithScreenshot] Algorithmia file %s exists: %s" % (uri, fileExists), 'blue')
			else:
				print colored("[compareImageWithScreenshot] Algorithmia file %s exists: %s" % (uri, fileExists), 'red')
			#if not fileExists:
			#	sys.exit(2)
			if os.path.exists(img) and fileExists is False:
				new_uri = self.runAlgorithmiaCommand(img)
			else:
				print "[compareImageWithScreenshot]  %s is not a file. or fileExists is %s" % (img, fileExists)
		
			#https://algorithmia.com/v1/data/andreaf/images/Matteo_Salvini_a_Otto_e_mezzo_su_La7_-_21-9-2015_Salvini_1905.0.jpg
			#https://algorithmia.com/v1/data/andreaf/images/MATTEO-SALVINI.png
			#input.append(["data:/"+enc_img])
		
			input.append(new_uri)
		#input = ["data://andreaf/images/MATTEO-SALVINI.png", "data://andreaf/images/Matteo_Salvini_a_Otto_e_mezzo_su_La7_-_21-9-2015_Salvini_1905.0.jpg"]
		#for i in input:
		#	print i[:100]
		#print len(encoded_images)
		algo = client.algo('zskurultay/ImageSimilarity/0.1.2')
		sim = algo.pipe(input)
		#colo "[compareImageWithScreenshot] %s %s" % (image1, image2)
		print colored("[compareImageWithScreenshot] similarity: %s" % sim, 'red')
		return sim

	def run(self, params = {}, main_dir = "/home/andrea/jobs/politify/SearchTweetsByKeywordOverTime/api/video_data"):
		if len(params) == 0:
			params = self.getFromDatabase()[0]
			#pprint.pprint(params)
		try:
			video_url = params['url']
			timecodes = params['play_events'][0]['play_events']
			video_title = params['title']
			video_keywords = params['play_events'][0]['person']
			images_location = main_dir+'/salvini/images'#params['images_location']
			frames_folder = main_dir + "/salvini/frames"
		except Exception as e:
			print "[compareImageWithScreenshot][run][ERROR] %s" % e
			pass
		video_location = self.getVideo(ref = video_url, file_name = main_dir+"/salvini/videos/"+video_title.replace(" ","_").replace("\"","").replace("/","-"))
		frames_ref = self.extractFrames(timecodes = timecodes, frames_folder=frames_folder, video_file_location = video_location, frame_name = video_title + "_" + video_keywords)
		images_paths = self.readImages(images_location)
		similarities = []
		for image in images_paths:
			for frame in frames_ref:
				sim_info = {
					"similarity": self.compareImageWithScreenshot(image, frame),
					"image": image,
					"frame": frame,
					"url": video_url,
					"video_keywords":video_keywords,
					"video_title":video_title,
					"timecodes": timecodes
				}
				similarities.append(sim_info)
				print colored("similarity between %s and %s is %s" % (image,frame, sim_info['similarity']), 'green')

		#pprint.pprint(similarities)

		# SAVING TO MONGODB HERE
		self.dbconn.addToMongo(similarities, obj_type = "similarities", coll = "similarities")
		return similarities

	def getFromDatabase(self):
		"""Read data from database with video information."""
		coll = "videos"
		v = self.dbconn.readAllByColl(coll)
		videos = loads(dumps(v))
		return videos


#def main():
	# video_url
	# timecodes
	# video_location
	# video_title
	# video_keywords
	# images_location
	#
			#	with open(img, "rb") as image_file:
			#		encoded_string = base64.b64encode(image_file.read())
			#		encoded_images.append(encoded_string)
			

	

if __name__ == "__main__":
    
    analyse_video = AnalyseVideo()
    analyse_video.run()