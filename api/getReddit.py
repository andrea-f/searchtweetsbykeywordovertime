#!/usr/bin/env python
import json
import requests
from pprint import pprint as pp2
import sys
from DINXutils import UtilitiesTools
import os
import traceback
dinx = os.path.abspath(os.path.join(os.path.dirname( __file__ ), '..', '..', '..', '..', 'djangoApp', 'dinx', 'db_operations'))
print dinx
sys.path.append(dinx)
from add import SaveEventsInDatabase as saveInDB
django = os.path.abspath(os.path.join(os.path.dirname( __file__ ), '..', '..', '..', '..', 'djangoApp'))
sys.path.append(django)
os.environ['DJANGO_SETTINGS_MODULE'] = 'dinx.settings'
#
# For information code@dinx.tv www.dinx.tv
#

class GenerateRedditData(object):

    def __init__(self):
        self.root = "Film ENG"

    def _login(self, username, password):
        """logs into reddit, saves cookie.

        :param username:
            Reddit registered user name, string.

        :param password:
            Password associated with user name, string.

        Return requests session client with Reddit login info after successfull login.
            
        """
        print '{GenerateRedditData} [login] begin REDDIT log in\n'
        try:
            UP = {'user': username, 'passwd': password, 'api_type': 'json',}
            headers = {'user-agent': '/u/Dinx Television\'s API python bot', }
            #POST with user/pwd
            client = requests.session(headers=headers)
            r = client.post('http://www.reddit.com/api/login', data=UP)
            #gets and saves the modhash
            j = json.loads(r.text)
            client.modhash = j['json']['data']['modhash']
            print '(GenerateRedditData) [login] {USER}\'s modhash is: {mh}'.format(USER=username, mh=client.modhash)
            client.user = username
            def name():
                return '{}\'s client'.format(username)
            return client
        except Exception as e:
            print "{GenerateRedditData} [login] Reddit login error %s" % e
            exc_type, exc_value, exc_traceback = sys.exc_info()
            error = "{GenerateRedditData} [login] Error whilst running HOST:\n\n%s" % traceback.format_exc(limit=10)
            print error

            
    def _subredditInfo(self, client, limit=25, sr='fullmoviesonyoutube',
                      sorting='', return_json=False, **kwargs):
        """Retrieves X (max 100) amount of stories in a subreddit.\n


        :param client:
            Requests session object returned after login in Reddit, requests.session object.

        :param limit:
            Specifies max number of stories returned from Reddit, int.

        :param sr:
            Specifies subreddit channel to fetch data from, string.

        :param sorting:
            For 'sorting' is whether or not the sorting of the reddit should be customized or not,
            if it is: Allowed passing params/queries such as t=hour, week, month, year or all, string.

        :param return_json:
            Wether to return json or list from Reddit, boolean.

        Return if return_json is True:
            * **j** -- JSON dict of subreddit channel information.

        Return if return_json is False:
            * **stories** -- List of subreddit channel information.

        """
        # Query to send
        parameters = {'limit': limit}
        #parameters= defaults.copy()
        parameters.update(kwargs)
        url = r'http://www.reddit.com/r/{sr}/{top}.json'.format(sr=sr, top=sorting)
        r = client.get(url,params=parameters)
        print 'sent URL is', r.url
        j = json.loads(r.text)
        # Return raw json
        if return_json:
            return j
        # Or list of stories
        else:
            stories = []
            for story in j['data']['children']:
                stories.append(story)
            return stories    

    def _saveReturnedVideos(self, videoList):
        """Display or save the returned video list from Reddit.

        :param videoList:
            Holds returned video information from Reddit, list.

        Return int with number of total saved events.

        """
        listOfEvents = self._createListOfEvents(videoList)
        try:
            saveindb = saveInDB(list_of_events = listOfEvents['info'], root = self.root)
            saveindb.saveEvents()
        except Exception as e:
            print "{GenerateRedditData} [saveReturnedVideos] Error in saving: %s" % e
        return len(listOfEvents)

    def _createListOfEvents(self, videoList):
        """This function creates a dictionary named info containing keys to match db input format.

        :param videoList:
            Holds returned video information from Reddit, list.

        Returns dict with key info containing list of dicts describing event. One dict for each event.
        
        """
        events = []
        for video in videoList:
            try:
                name = video['data']['title']
                #put this in utils
                ut = UtilitiesTools()
                date = ut.getDateFromName(name)
                root = self.root
                title = video['data']['title']
                viewcount = video['data']['ups']
                duration = 2300
                thumbnail = video['data']['media']['oembed']['thumbnail_url']                
                try:
                    keywords = video['data']['media']['oembed']['keywords']
                except Exception as e:
                    keywords = ""
                try:
                    description = video['data']['media']['oembed']['description']
                except Exception as e:
                    description = ""
                #"http://www.youtube.com/v/b4K4597mLJg?version=3&f=videos&app=youtube_gdata"
                #http://www.youtube.com/watch?v=CZSnygXjHSY
                url = video['data']['media']['oembed']['url']
                url = ut.getVideoId(url)
                try:
                    event = {
                        "category": "movies",
                        "name": name,
                        "videos": [{
                            "viewcount": viewcount,
                            "description": description,
                            "title": title,
                            "url": url,
                            "keywords": keywords,
                            "score": 1000,
                            "duration": duration,
                            "thumbnail": thumbnail
                        }],
                        "items": [],
                        "helper_words": ["reddit"],
                        "normalized": "",
                        "date": date,
                        "root": root
                    }
                    print event['name'], event['date']
                    events.append(event)
                except Exception as e:
                    print "{GenerateRedditData} [createListOfEvents] error in creating event %s" % e
            except Exception as e:
                print "{GenerateRedditData} [createListOfEvents] Video has been removed: %s" % e
            listOfEvents = {"info": events}
        return listOfEvents

    def getRedditData(self):
        """This function gets full youtube movies from Reddit. Then transforms data into dinx compatible Event dicts, then saves this information in DB.

        Return int with total number of saved videos.
        
        """
        client = self._login('dinxtv', 'bandanoantri88')
        j = self._subredditInfo(client, limit=200)
        saved = self._saveReturnedVideos(j)
        totalVideos = len(j)
        lastReturnedVideo = j[totalVideos - 1]['data']['name']
        try:
            while totalVideos >= 100:
                y = self._subredditInfo(client, limit=200, after=lastReturnedVideo)
                lastReturnedVideo = y[len(y) - 1]['data']['name']
                saved = saved + self._saveReturnedVideos(y)
        except:
            pass            
        return saved

def main():
    grd = GenerateRedditData()
    saved = grd.getRedditData()

if __name__ == "__main__":
    main()