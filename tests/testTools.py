# To change this template, choose Tools | Templates
# and open the template in the editor.

#code provided by www.dinx.tv code is under bsd license
import unittest
import os, sys
from pprint import pprint

lib = os.path.abspath(os.path.join(os.path.dirname( __file__ ),'..','api'))
sys.path.append(lib)
lib1 = os.path.abspath(os.path.join(os.path.dirname( __file__ ),'..'))
sys.path.append(lib1)

from YouTube import SearchVideos
from mongo import MongoDB
import pymongo
from PolitifyEngine import PolitifyEngine


class TestPolitifyTestCase(unittest.TestCase):
    
    def setUp(self):
    	self.youtube = SearchVideos()
    	self.conn = MongoDB()
    	self.pe = PolitifyEngine()

    def tearDown(self):
        """"""
    #    self.foo = None
    	self.youtube = None
    	self.conn = None

    def mockInput(self):

	return [{
        	"title":"Otto e Mezzo",
        	"date": "09-05-2015",
        	"url":"https://www.youtube.com/watch?v=JqNhTf2VFqY",
        	"cp":"la7",
        	"start_time":"20:30:00",
        	"keywords":["Cocchi"],
	        "description":"Ospiti Salvatore Tramontano (vicedirettore Il Giornale), Davide Faraone (Sottosegretario Ministero dell Istruzione) e Giovanni Cocchi (insegnante)."
	}
        ]

    def testYouTubeWithQuery(self):
        """Tests retrieval of a set of videos from YouTube by query."""
        prog = self.mockInput()
        query = "%s %s" % (prog['title'], prog['cp'])
        feed = self.youtube.runYouTubeQuery(query)
        videos = []
        for entry in feed: 
        	vid = self.youtube.createVideoObject(
        		entry = entry,
        		cp = prog['cp'],
        		date = prog['date'],
        		extrawords = prog['keywords'], 
        		title = prog['title'],
        		program_description = prog['description']
        	)

        	videos.append(vid)
        print len(videos)
        # Save retrieved videos in db?

    def testDrawFigureWithCustomTimeInterval():
        params = {
            "keywords" : keywords,
            "language" : "it",#,
            "min_tweets":1,
            "set_date_range":set_date_range,
            "until":until,
            "since":since,
            "date": {
                "day":03,
                "month": 10,
                "year": 2015,
                "start_time": 22,
                "end_time": 24,
                "time_interval": 5

            }
            #"date":1425341144
        }
        st = Politify(params = params)
        tweets = st.searchTwitter(actual_request = actual_request)
        tweets_by_time = tweets["tweets_by_time"]
        tweets_by_day = tweets["tweets_by_week"]
        st.saveTweetsAndReport(tweets_by_day, tweets_by_time)
        #st.plotOverDays(tweets_by_day)
        #st.showRelevantTimes(tweets_by_day)
        
            
    def testApplyFilter(self):
    	"""Apply a filter to the video feed"""

    def testDateRangeAndKeyword(self):
    	"""
    	db.entries.find({
    		"time":	{
    			"$gte":1431126000,
    			"$lte":1431212400
    		},
    		"keywords":['cocchi']
    	})
		"""
    	entries = self.pe.readData(word = 'cocchi', date = "09-05-2015")
    	self.assertTrue(isinstance(entries.count(),int))
    	self.assertTrue(isinstance(entries,pymongo.cursor.Cursor))

    def testGetVideoObject(self):
    	"""Testst video retrieval

    	:param prog:
    		Hold info about input object, list.
    	"""
    	prog = self.mockInput()
    	video = self.pe.getVideoObject(prog = prog[0], entry = {})
    	mts = dir(video)
    	for m in mts:
    		if hasattr(video,m):
    			print m, getattr(video,m)
    	self.assertTrue(isinstance(video.url,unicode))
    	if len(video.url)==0:
    		self.fail('No url retrieved.')
    	

    def fetchTweetsInDay(self):
    	"""Retrieves tweet in a day"""
    	prog = self.mockInput()
    	pe = PolitifyEngine(record = prog)
    	video_with_tweets = pe.run(save = True, silent = True)
    	
    	if not video_with_tweets is None:
	    	self.assertTrue(isinstance(video_with_tweets.__dict__, dict), msg = "%s is not a list(items)." % type(video_with_tweets.items))
    		self.assertTrue(isinstance(video_with_tweets.title, unicode), msg = "%s is not a string(title)" % type(video_with_tweets.title))
    	else:
    		self.fail("video_with_tweets is None.")
    	#guessed_start_time =self.politifyengine.extractStartTime(tweets_by_day)
        #https://www.youtube.com/watch?v=aCLQ03nepLc

    

if __name__ == '__main__':

    suite = unittest.TestSuite()
    suite.addTest(TestPolitifyTestCase('fetchTweetsInDay'))
    #testDateRangeAndKeyword
    #suite.addTest(TestPolitifyTestCase('testDateRangeAndKeyword'))
    
    #suite.addTest(TestPolitifyTestCase('testGetVideoObject'))
    #suite.addTest(TestToolsTestCase('testCalculateTfIdf'))
    #suite.addTest(TestURLRetrivalTestCase('testGrammarCheck'))

    unittest.TextTestRunner().run(suite)

