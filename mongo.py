from pymongo import Connection
import os, sys, traceback
from os import listdir
from os.path import isfile, join
import hashlib
import re

class MongoDB():
	
	def __init__(self,):
		connection = Connection()
		self.db = connection['politify-database']
		self.collection = self.db['politify-collection']
		#indexes = self.collection.getIndexes()
		#print indexes
		#self.collection.createIndex( { "hash": 1 }, { unique: true, sparse: true } )
	

	def filterTweets(self, word = "", date = 0):
		try:
			word = word.lower()
		except:
			pass
		entries = self.db.entries
		one_day = 92600
		first = date
		last = date+one_day
		print "[MongoDB][filterTweets] date, from %s to %s for %s" % (first, last, word)
		if len(word) == 0 and date ==0:
			res = entries.find()
		elif len(word) > 0 and date ==0:
			res = entries.find({"keywords":word})
		elif len(word) == 0 and date !=0:
			res = entries.find({"time": {"$gte": first, "$lte": last}})
		elif len(word) > 0 and date !=0:
			res = entries.find({"keywords":re.compile('^' + re.escape(word) + '$', re.IGNORECASE), "time": {"$gte": first, "$lte": last}})
			#re.compile('^' + re.escape(word) + '$', re.IGNORECASE)
		
		print "[MongoDB][filterTweets] Retrieved records: %s" % res.count()
		return res

	


	def readFolder(self,dataDir = None):
		if dataDir is None:
			dataDir = self.dataDir
		fs = []
		for dirname, dirnames, filenames in os.walk(dataDir):
			for subdirname in dirnames:
				d = os.path.join(dirname, subdirname)
				for f in os.listdir(d):	fs.append(d+"/"+f)
			for filename in filenames:
				fs.append(os.path.join(dirname, filename))
		#fs = [ f for f in listdir(dataDir) if isfile(join(dataDir,f)) ]
		return fs

     
	def hashSentence(self, sent = "Any sentence would do here!", hashLength = 16):
		"""Generates MD5 hash for sentence, so only unique sentences are saved.

		:param sent:
			Text to generate hash from, string.

		:param hashLength:
			Max length of generated hash, int.

		Return hex int with sent hash.
		"""
		hash = int(hashlib.md5(sent).hexdigest(), hashLength)
		return hash

	def mergeTweet(self, tweet):
		sent = ""
		for k,v in tweet.items():
			try:
				sent+=v.encode('ascii', errors='ignore')
			except:
				sent+=str(v)
		return sent			

	def _saveInDB(self, coll, data_dict, hash_sent = "", obj_type = ""):
		"""Does the insert in collection."""
		if len(hash_sent) == 0:
			sent = ""
			try:
				try:
					sent = data_dict['url'].encode('ascii','ignore') + data_dict['description'].encode('ascii','ignore')  + data_dict['extra_words'][0].encode('ascii','ignore')  
				except Exception as e:
					print "[_saveInDB] Saving anonym dict"
					#sent = str(data_dict).encode('ascii','ignore')
					sent = ''.join('{}{}'.format(key, val) for key, val in data_dict.items())
				hash_sent = str(self.hashSentence(sent))
				#print "hash: %s, str: %s" % (hash_sent, sent)
				data_dict['hash'] = hash_sent
			except Exception as e:
				print "[ERROR][_saveInDB] convert to hash: %s" % e
		try:
			total_records = list(coll.find({"hash":hash_sent}))
			how_many_exists = len(total_records)
			if how_many_exists==0:
				print "Saving new item..."
				entry_id = coll.insert(data_dict)
			elif how_many_exists>0 and ("video" in obj_type) or ("program" in obj_type):
				print "Saving updated item..."
				
				entry_id = coll.update({"url":data_dict['url']},data_dict)
			return 1
			
		except Exception as e:
			print "[Error] in saving entry: %s" % e
			exc_type, exc_value, exc_traceback = sys.exc_info()
			error = "[Error]:\n\n%s" % traceback.format_exc(limit=10)
			print error  
		return 0
						
	def readAllByColl(self, coll):
		"""Read all records by collection"""
		try:
			entries = self.db[coll].find()
		except Exception as e:
			print "[readAllByColl] Error %s" % e
		return entries
	def addToMongo(self, tweets, obj_type = "twitter", available_conn_types = ["tweets", "video"]):
		"""
		:param tweets:
			Dicts with tweet information details, list.

		:param obj_type:
			what to save, options format for MongoDB connection, string.
			possible values: ['tweets', 'video']

		entry = {
			"day":"1428267191",
			"times":[1428267191,1428267191,1428267191],
			"keywords": "buonanno"
			"username": tweet['username'],
			"text": tweet["text"],
			"date": tweet ["date"],
			"keywords": tweet['keywords']
			}
		"""
		is_time = False
		try:
			if isinstance(tweets[0], tuple):
				is_time = True
				coll = self.db.times 
		except:
			pass			
			
		added = 0
		try:
			if "twitter" in obj_type:
				coll = self.db.entries
				v = list(tweets)
				for tweet in tweets:
						if is_time:
							sent = "%s-%s-%s"%(tweet[0], ','.join(str(v) for v in tweet[1]), str(tweet[2]))
							hash_sent = str(self.hashSentence(sent))
							tweet = {
								"day": tweet[0],
								"times": tweet[1],
								"keywords": tweet[2]
							}
						else:
							sent = self.mergeTweet(tweet)
							hash_sent = str(self.hashSentence(sent))					
							tweet['hash'] = hash_sent 
						try:
							added+=self._saveInDB(coll,tweet,hash_sent)
							print "%s Added: %s" % (entry_id,tweet['time'])
						except:
							pass
			elif "video" in obj_type:
				coll = self.db.videos
				videos = tweets
				video_dict = {}
				#print videos[0].__dict__
				for video in videos:
					try:
						added+=self._saveInDB(coll,video.__dict__, obj_type = "video")
						print "Added: %s" % (video.title)
					except Exception as e:
						print "[ERROR][addToMongo] Low level save: %s" % e
						print "[Error] in saving entry: %s" % e
						exc_type, exc_value, exc_traceback = sys.exc_info()
						error = "[Error]:\n\n%s" % traceback.format_exc(limit=10)
						print error
			elif "program" in obj_type:
				coll = self.db.programs
				inputs = tweets
				for inp in inputs:
					try:
						added += self._saveInDB(coll, inp[0], obj_type = "program")
					except Exception as e:
						print "[ERROR][addToMongo] Low level %s save: %s " % (obj_type,e)
						print "[Error] in saving entry: %s" % e
						exc_type, exc_value, exc_traceback = sys.exc_info()
						error = "[Error]:\n\n%s" % traceback.format_exc(limit=10)
						print error
		except Exception as e:
			print "[ERROR][addToMongo] %e" % e			

		print "Added %s records to DB" % added
		return added
if __name__ == '__main__':
	try:
		dataDir = sys.argv[1]
	except:
		dataDir = os.path.abspath(os.path.join(os.path.dirname( __file__ ), '..', 'data', 'papers'))
	print dataDir
	mg = MongoDB()
	#fs = mg.readFolder(dataDir)
	#mg.addToMongo(fs)
	#db.entries.find({"text":{"$regex":"Evans"}})
	mg.readFromMongo()