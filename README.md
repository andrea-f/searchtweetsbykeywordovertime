# SearchTweetsByKeywordOverTime
SearchTweetsByKeywordOverTime is a python 2.7 script which uses matplotlib to plot tweets containing a specific keyword over a custom time interval. Also it provides filtering by frequency of tweets overtime meaning that a higher number of tweets in a short time are labeled whereas periods of time with low tweet frequency are skipped. Front end is in React. 

Below a screenshot of running the app on localhost populated with the analysis of one televised political debate on Italian TV. Clicking on a salient point in ms, will seek to that point in the video:

![politify3](https://bitbucket.org/andrea-f/searchtweetsbykeywordovertime/raw/f20f0284d313bc22ded21b47ce0485a74d328a40/tweets_time_by_keyword/sample_output.png)

High res: https://bitbucket.org/andrea-f/searchtweetsbykeywordovertime/src/master/tweets_time_by_keyword/sample_output.png 

## 0. Config and prerequisites
Install requirements after creating and activating a virtualenv with: `pip install -r requirements`.

Make surean instance of `MongoDB` is running on the standard location `localhost:27017`.

Config used is in `input.json`
Edit `input.json` to change to a different TV program:
```
[
    {
        "description": "Non \u00e8 l'Arena di Massimo Giletti.", 
        "language": "it", 
        "title": "Non \u00e8 l'Arena", 
        "url": "https://www.youtube.com/watch?v=YO7VUlnFSl0", 
        "start_time": "20:30:00", 
        "date": "31-03-2019", 
        "end_time": "02:10:00", 
        "min_tweets": 9, 
        "keywords": [
            "nonelarena"
        ], 
        "cp": "la7"
    }
]
```

## 1. Fetch tweets by keyword in time
Engine matches tweets of a program and a keyword in a specified timeframe.

In root directory, run: `python tw_grab.py`.

Will produce (if everything goes well :)

- 1 png file in webapp/graphs folder

- 1 text file with time of tweet per line


## 2. Run politify engine, data used for engine is first entry of input.json
Matches video time with tweets time.
Run engine with: `python PolitifyEngine.py`.

Populates the MongoDB and will produce a json file in `tweets_time_by_keyword/output_data/` with the item representation of the saved video in the db.

Sample produced JSON file: https://bitbucket.org/andrea-f/searchtweetsbykeywordovertime/src/master/tweets_time_by_keyword/output_data/Non%20%C3%A8%20l'Arena_31-03-2019_la7_nonelarena.json

## 3. Run API server
Runs a simple HTTP server on `localhost:8989`: `sudo python server.py`

## 4. Run front end application
Serve, using your own custom server, the React app from `webapp` folder and then access the HTTP location at `index.html`.


# Sample output

1. Get operating times of tv show
2. Find tv show on youtube
3. Match twitter stream for specific person/program/event
4. Looking at the spikes of tweets by that person/event shows an indication of the salient points during the debate.



